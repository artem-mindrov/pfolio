/*
 *   Character Count Plugin - jQuery plugin
 *   Dynamic character count for text areas and input fields
 *    written by Alen Grakalic
 *    http://cssglobe.com/
 *
 *
 * Updated to work with Twitter Bootstrap 2.0 styles by Shawn Crigger <@svizion>
 * http://blog.s-vizion.com
 * Mar-13-2011
 *
 *   Copyright (c) 2009 Alen Grakalic (http://cssglobe.com)
 *   Dual licensed under the MIT (MIT-LICENSE.txt)
 *   and GPL (GPL-LICENSE.txt) licenses.
 *
 *   Built for jQuery library
 *   http://jquery.com
 *
 *
 */

(function($) {

  $.fn.limit = function(options){

      // default configuration properties
      var defaults = {
          allowed: 140,
          warning: 25,
          css: 'help-inline',
          counterElement: 'span',
          cssWarning: 'warning',
          cssExceeded: 'danger',
          counterText: "bootstrap.limit.counter_text",
          val: "val"
      };

      var options = $.extend(defaults, options);

      function calculate(obj){
          var count, counterText;

          if (options.words === true) {
              counterText = options.counterText + ".words";

              if ($(obj)[options.val]() == null || $(obj)[options.val]().match(/\w+/g) == null) {
                  count = 0;
              } else {
                  count = $(obj)[options.val]().match(/\w+/g).length;
              }
          } else {
              counterText = options.counterText + ".chars";
              count = $(obj)[options.val]().length;
          }

          var available = options.allowed - count;
          if(available <= options.warning && available >= 0){
//Since the error/warning field is actually in the first div, we jump 2 parent classes up to find it.
              $(obj).next().parent().parent().addClass(options.cssWarning);
          } else {
              $(obj).next().parent().parent().removeClass(options.cssWarning);
          }
          if(available < 0){
              $(obj).next().parent().parent().addClass(options.cssExceeded);
          } else {
              $(obj).next().parent().parent().removeClass(options.cssExceeded);
          }
          $(obj).next().html(t(counterText, {count: (available < 0) ? 0 : available}));
      };

      this.each(function() {
          $(this).after('<'+ options.counterElement +' class="' + options.css + '">'+ options.counterText +'</'+ options.counterElement +'>');
          calculate(this);
          $(this).keyup(function(){calculate(this)});
          $(this).change(function(){calculate(this)});
      });

  };

})(jQuery);

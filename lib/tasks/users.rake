namespace :users do
  task make_admin: :environment do
    require "highline/import"

    print "Email of the admin user: "
    email = $stdin.gets.chomp
    password = ask("Password: ") {|q| q.echo = "*"}
    password_confirmation = ask("Confirm: ") {|q| q.echo = "*"}

    User.skip_callback(:create)
    admin = User.create!(email: email, password: password, password_confirmation: password_confirmation, approved: true)
    admin.add_role(:admin)
    User.set_callback(:create)
  end
end
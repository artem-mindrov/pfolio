class DeviseFailure < Devise::FailureApp
  def redirect_url
    #return super unless [:worker, :employer, :user].include?(scope) #make it specific to a scope
    root_path
  end

  def respond
    http_auth? ? http_auth : redirect
  end
end
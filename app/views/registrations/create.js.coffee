role = "<%= resource.roles.first.name %>"

<% if resource.valid? %>
$(".modal").modal('hide')
$("#thankyou-#{role}").modal('show')
<% else %>
$("#new_#{role}").replaceWith('<%= j render("landing_page/#{resource.roles.first.name}") %>')
<% end %>
json.results do
  json.array!(@users) do |user|
    json.id user.id
    json.name user.to_s
  end
end

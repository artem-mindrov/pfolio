$("#triggered-events li").remove()
$("#triggered-events-placeholder").show()
videojs($("#current-chapter video").attr("id")).dispose()

$("#current-chapter").html('<%= j render("enrolments/current_chapter", chapter: @current_chapter) %>')
$("#chapter-list li.current").removeClass("current")
$("#item-<%= dom_id(@current_chapter) %>").addClass("current")
videoid = "#<%= dom_id(@current_chapter)%>-video"

setup = $(videoid).data("setup")
try
  setup = JSON.parse(setup)
catch e
  setup = {}

videojs(videoid, setup)
initEvents(<%= @current_chapter.gaming_events.discovered(@enrolment).to_json.html_safe %>,
  <%= @current_chapter.gaming_events.undiscovered(@enrolment).to_json.html_safe %>,
  '<%= dom_id(@current_chapter) %>')
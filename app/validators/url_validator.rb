class UrlValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    valid = begin
      URI.parse(value).kind_of?(URI::HTTP)
    rescue URI::InvalidURIError
      false
    end

    unless valid
      record.errors[attribute] << (options[:message] || I18n.t("activerecord.errors.models.user.attributes.website.invalid"))
    end
  end
end
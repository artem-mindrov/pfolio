class UserMailer < ActionMailer::Base
  def notify_approved(user_id)
    mail to: User.find(user_id).email
  end

  def thanks(user_id)
    mail to: User.find(user_id).email
  end
end

class AdminMailer < ActionMailer::Base
  default(to: Proc.new { User.with_role(:admin).pluck(:email) })

  def new_user_notify(user_id)
    @user = User.find(user_id)
    mail
  end
end

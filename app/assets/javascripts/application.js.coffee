# This is a manifest file that'll be compiled into application.js, which will include all the files
# listed below.
#
# Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
# or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
#
# It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
# the compiled file.
#
# WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
# GO AFTER THE REQUIRES BELOW.
#
#= require jquery
#= require jquery_ujs
#= require bootstrap
#= require tween
#= require jquery.scrolltofixed.min
#= require medium-editor
#= require underscore
#= require backbone
#= require backbone.syphon
#= require backbone.modelbinder
#= require paloma
#= require select2
#= require tmpl
#= require jquery.jcrop
#= require fileupload/vendor/jquery.ui.widget
#= require canvas-to-blob
#= require load-image
#= require fileupload/jquery.fileupload
#= require fileupload/jquery.fileupload-ui
#= require fileupload/jquery.fileupload-process
#= require fileupload/jquery.fileupload-validate
#= require fileupload/jquery.iframe-transport
#= require load-image-meta
#= require load-image-exif
#= require fileupload/jquery.fileupload-image
#= require fileupload/jquery.fileupload-video
#= require jquery_nested_form
#= require bootstrap.limit
#= require loading-overlay
#= require video.dev
#= require jquery.sortable
#= require biginteger
#= require popcorn
#= require bootstrap-slider
#= require timeline
#= require s3_multipart/lib
#= require validation-engine/languages/jquery.validationEngine-en
#= require validation-engine/jquery.validationEngine
#= require ./backbone/picct
#= require_tree ./paloma
#= require_tree ./base

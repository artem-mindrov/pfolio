EssaysController = Paloma.controller("Essays")

EssaysController::new = EssaysController::edit = EssaysController::create = EssaysController::update = ->
  author = $("input[id$=author_id]").data("author")

  $("input[id$=author_id]").select2
    minimumInputLength: 3
    formatResult: (item, page) -> item.name || item.email
    formatSelection: (item, page) -> item.name || item.email
    initSelection: (el, cb) -> author && cb(author)
    ajax:
      url: "/users"
      dataType: "json"
      data: (term, page) -> q: term
      results: (data, page) -> data

  tags = $("input[id$=category_list]").data("tags")

  $("input[id$=category_list]").select2
    tags: true
    minimumInputLength: 3
    initSelection: (el, cb) -> tags && cb(tags)
    formatResult: (item, page) -> item.name
    formatSelection: (item, page) -> item.name
    createSearchChoice: (term, data) ->
      if ($(data).filter( -> @name.localeCompare(term) is 0 ).length is 0)
        { id: term, name: term }
    ajax:
      url: "/tags"
      dataType: "json"
      data: (term, page) -> q: term
      results: (data, page) -> results: data

  setSelect2Hooks()

  text = $(":input[id$=text]")
  editable text
  limit = text.data("limit")
  $(".editable").limit
    allowed: limit
    warning: limit / 4
    css: "help-block"
    counterElement: "div"
    words: true
    val: "text"

  $("form").validationEngine(validateNonVisibleFields: true)

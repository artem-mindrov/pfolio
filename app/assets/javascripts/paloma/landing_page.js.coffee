LandingPageController = Paloma.controller("LandingPage")

LandingPageController::index = ->
  window.twttr = ((d, s, id) ->
    js = undefined
    fjs = d.getElementsByTagName(s)[0]

    unless d.getElementById(id)
      js = d.createElement(s)
      js.id = id
      js.src = "//platform.twitter.com/widgets.js"
      fjs.parentNode.insertBefore js, fjs
      window.twttr || (t = { _e: [], ready: (f) -> t._e.push(f) })
  ) document, "script", "twitter-wjs"

  $("form").validationEngine()

  $("input[name*=website]").focus ->
    $(@).val("http://") if $(@).val() == ""

  $("input[name*=website]").blur ->
    $(@).val("") if $(@).val().replace(/^https?:\/\//, "") == ""

  $("#thankyou-student a.tweet").attr("href", "http://twitter.com/intent/tweet?url=#{encodeURIComponent(location.href)}&text=#{encodeURIComponent("Signed up for filmmaking courses at #{location.href}")}")
  $("#thankyou-creative a.tweet").attr("href", "http://twitter.com/intent/tweet?url=#{encodeURIComponent(location.href)}&text=#{encodeURIComponent("Teach filmmaking at #{location.href}")}")

  twttr.ready (twttr) ->
    twttr.events.bind "tweet", (e) ->
      if e.target.className.match(/tweet/)
        $(".share-status:visible").addClass("alert-success").text(t("landing_page.index.thanks_for_sharing"))

  $(".fb-share").click (e) ->
    e.preventDefault()
    u = "http://picct.me"
    t = document.title
    fullUrl = 'https://www.facebook.com/sharer.php?u=' + encodeURIComponent(u) + '&t=' + encodeURIComponent(t)+'&i=' + encodeURIComponent(i)
    window.open(fullUrl, 'sharer', 'toolbar=0,status=0,width=626,height=436')
    false
RegistrationsController = Paloma.controller("Registrations")

RegistrationsController::edit = ->
  for field in [ $("#user_bio"), $("#user_name"), $("#portfolio textarea[name$='[description]']")]
    max = field.data("max")
    field.limit(allowed: max, warning: max/8)

  $("#avatar-uploader").fileupload
    url: "/users/avatar"
    dataType: "script"
    uploadTemplateId: null
    downloadTemplateId: null
    previewMaxWidth: 256
    previewMaxHeight: 256
    acceptFileTypes: /^image\/(gif|jpe?g|png)$/i
    disableImageResize: /Android(?!.*Chrome)|Opera/
        .test(window.navigator && navigator.userAgent)
    imageMaxWidth: 256
    imageMaxHeight: 256

    done: (e, data) ->
      fu = $(@).data('blueimp-fileupload') || $(@).data('fileupload')
      getFilesFromResponse = data.getFilesFromResponse || fu.options.getFilesFromResponse
      files = getFilesFromResponse(data)

      if data.context
        data.context.each (index) ->
          file = files[index] || { error: 'Empty file upload result' }
          deferred = fu._addFinishedDeferreds()
          data.context = $(@)
          fu._trigger('completed', e, data)
          fu._trigger('finished', e, data)
          deferred.resolve()
      else
        deferred = fu._addFinishedDeferreds()
        data.context = $(@)
        fu._trigger('completed', e, data)
        fu._trigger('finished', e, data)
        deferred.resolve()

    add: (e, data) ->
      file = data.files[0]
      fu = $(@).data("blueimp-fileupload") || $(@).data("fileupload")

      $(@).fileupload('process', data).done ->
        # see _hasError() in jquery-fileupload-ui
        unless @options.acceptFileTypes.test(file.type) or @options.acceptFileTypes.test(file.name)
          file.error = t("fileupload.errors.type_not_allowed")

        if @options.maxFileSize and @options.maxFileSize < file.size
          file.error = t("fileupload.errors.file_too_big", size: formatFileSize(@options.maxFileSize))

        data.files.valid = data.isValidated = if file.error then false else true
        result = tmpl("avatar-template-upload", file)
        data.context = $(@options.templatesContainer).html(result).children().data("data", data)
        fu._renderPreviews(data)
        fu._transition(data.context)

        $("#avatar-uploader .image-preview").html(data.context)

        canvas = data.context.find("canvas")
        submitBtn = $("#cropped-preview button[type=submit]")

        w = canvas.width()
        h = canvas.height()

        if w != h
          updatePreview = (c) ->
            ww = Math.abs(c.x2 - c.x)
            hh = Math.abs(c.y2 - c.y)

            if ww and hh
              $("#cropped-preview img").hide()
              dest = $("#cropped-preview canvas").show()[0]
              dest.getContext("2d").drawImage(canvas[0], c.x, c.y, ww, hh, 0, 0, dest.width, dest.height)
              submitBtn.removeAttr("disabled")

          s = if w < h then w else h

          canvas.Jcrop
            onChange: updatePreview
            onSelect: updatePreview
            setSelect: [0, 0, s, s]
            aspectRatio: 1
        else
          $("#cropped-preview img").hide()
          dest = $("#cropped-preview canvas").show()[0]
          dest.getContext("2d").drawImage(canvas[0], 0, 0, dest.width, dest.height)

        submitBtn.removeAttr("disabled")

        submitBtn.click (e) ->
          $(@).attr("disabled", "disabled")
          e.preventDefault()
          src = $("#cropped-preview canvas")[0]
          mimeType = src.toDataURL().split(",")[0].match(/image\/.*?;/)[0].slice(0,-1)

          src.toBlob (blob) ->
            data.files[0] = blob
            $("#cropped-preview .progress").show()

            data.submit().error (jqXHR, textStatus, errorThrown) ->
              submitBtn.removeAttr("disabled")
              $("#cropped-preview .progress").hide()
          , mimeType

  $("#dropzone").on "click", (e) ->
    if e.target.id == "dropzone" or $(e.target).hasClass("image-preview") or $(e.target).hasClass("preview")
      $("#avatar-uploader input[type='file']").trigger('click')

  canvas = $("#cropped-preview canvas")[0]
  canvas.width = canvas.height = 128

  $("form").validationEngine(validateNonVisibleFields: true)
  $("form").on "jqv.form.result", ->
    $(@).find(".formError").closest(".panel").addClass("panel-danger")

CoursesController = Paloma.controller("Courses")

CoursesController::new = ->
  course = new Picct.Models.Course(@params.course)
  chapters = course.course_chapters

  if chapters?
    chapters.course = course
  else
    chapters = new Picct.Models.CourseChapters(course: course)

  new Picct.Views.Courses.Edit(model: course)
  chapters.each (chapter) -> new Picct.Views.CourseChapters.Edit(model: chapter)
  new Picct.Views.CourseChapters.Sortable(container: "#sortable-chapters", collection: chapters)

  $("#upload-status, .progress-header").hide()

CoursesController::show = ->
  $("a[rel=popover]").popover(
    placement: "auto bottom"
    trigger: "click"
  ).click (e) -> e.preventDefault()

CoursesController::watch = ->
  enrolment = @params.enrolment

  renderEvent = (event) ->
    $("#triggered-events-placeholder").hide()
    $("#triggered-events").append("<li>#{event.text}</li>")

  window.initEvents = (discovered, undiscovered, target) ->
    window.pop.destroy() if window.pop?
    window.pop = Popcorn("#" + $("#pc-#{target} video").attr("id"))

    handlers = $._data($("#main")[0], "events").click

    _.each discovered, (event) -> renderEvent(event)

    _.each undiscovered, (event) ->
      pop.footnote
        start: event.start
        end: event.start + event.duration
        text: "<div id='fn-#{event.id}' class='popcorn-footnote' style='top: #{event.top}%; left: #{event.left}%; cursor: pointer;'></div>"
        target: "##{target}-video"

      hoverTween = new TWEEN.Tween(scale: 1)
        .to( scale: 1.1, 300 )
        .easing( TWEEN.Easing.Cubic.InOut )
        .repeat( Infinity )
        .yoyo(true)
        .onUpdate ->
          $("#fn-#{event.id}").css(transform: "scale(#{@scale},#{@scale})")

      unless _.some(handlers, (h) -> h.selector == "#fn-#{event.id}")
        $("#main").on 'mouseover', "#fn-#{event.id}", ->
          hoverTween.start()

        $("#main").on 'mouseout', "#fn-#{event.id}", ->
          hoverTween.stop()

        $("#main").on 'click', "#fn-#{event.id}", (e) ->
          e.preventDefault()

          bin = $("#triggered-events")
          parent = $(@).parent().parent()
          dest =
            x: bin.offset().left + bin.width() / 2 - parent.offset().left
            y: bin.offset().top - parent.offset().top

          new TWEEN.Tween(x: $(@)[0].offsetLeft, y: $(@)[0].offsetTop)
            .to( dest, 700 )
            .easing( TWEEN.Easing.Back.In )
            .onUpdate( ->
                $("#fn-#{event.id}").css(left: @x, top: @y)
            ).chain(

            new TWEEN.Tween(scale: 1, opacity: 1)
              .to({scale: 0, opacity: 0}, 500)
              .easing( TWEEN.Easing.Cubic.In )
              .onUpdate( ->
                $("#fn-#{event.id}").css(opacity: @opacity, transform: "scale(#{@scale}, #{@scale})")
              ).onComplete ->
                renderEvent(event)
                notifier = $("<div class='event-notifier'></div>")
                css =
                  left: dest.x - notifier.width() / 2
                  transform: "scale(0,0)"

                if $(window).scrollTop() + $(window).height() > bin.offset().top
                  css.top = dest.y - notifier.height()
                else
                  css.position = "fixed"

                  if $(window).scrollTop() > bin.offset().top
                    css.transform += " rotate(180deg)"
                    css.top = 0
                  else
                    css.top = $(window).height() - notifier.height()

                notifier.css(css).appendTo(parent)

                new TWEEN.Tween(scale: 0)
                  .to( scale: 1, 400 )
                  .easing( TWEEN.Easing.Elastic.InOut )
                  .repeat(1)
                  .delay(400)
                  .yoyo(true)
                  .onUpdate( ->
                    notifier.css(transform: "scale(#{@scale},#{@scale})")
                ).start()
          ).start()

          $.ajax "/gaming_events/#{event.id}/discover",
            type: "post"
            dataType: "json"
            data:
              enrolment_id: enrolment.id

          false

  animate = ->
    requestAnimationFrame( animate )
    TWEEN.update()

  animate()
  initEvents(@params.discovered, @params.undiscovered, @params.target)

  $("a").on "ajax:beforeSend", ->
    $("#current-chapter").parent().loadingOverlay()

  # some of the links are replaced so ajax:complete and ajax:success never fire
  $(document).ajaxComplete (e,xhr,options) ->
    $("#current-chapter").parent().loadingOverlay("remove")
    if xhr.status == 200 && options.url.match(/watch/)
      history.pushState {}, '', options.url

  $("#chapter-list li a").click (e) ->
    item = $(@).closest("li")
    if item.hasClass("incomplete") || item.hasClass("current")
      e.preventDefault()
      false

  unless enrolment.completed_at
    $("#main").on "ended", "video", ->
      item = $("#chapter-list li.current").next("li")
      $("#next-chapter").removeClass("hide")
      if item.hasClass("incomplete") || item.length == 0
        $.post "/enrolments/#{enrolment.id}/progress",
          dataType: "script"
          success: (data, status, xhr) -> item.removeClass("incomplete")

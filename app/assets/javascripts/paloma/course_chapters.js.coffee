CourseChaptersController = Paloma.controller("CourseChapters")

CourseChaptersController::index = ->
  course_id = @params.course_id
  chapters = new Picct.Models.CourseChapters(@params.chapters)

  chapters.each (chapter) -> new Picct.Views.CourseChapters.Edit(model: chapter)
  new Picct.Views.CourseChapters.Sortable(container: "#sortable-chapters", collection: chapters)

  $(".save-all").click ->
    $(@).attr("disabled", "disabled")
    forms = $("#sortable-chapters .main-form form")

    delayedSubmit = (idx) =>
      if 0 <= idx < forms.length
        $(forms[idx]).find(":submit").trigger("click")
        setTimeout ( -> delayedSubmit(++idx) ), 1000
      else if idx == forms.length
        $(@).removeAttr("disabled")

    delayedSubmit(0)

  $("#add-chapter").click ->
    chapters.add( new Picct.Models.CourseChapter(course_id: course_id) )

  $(".progress-header").hide()

CourseChaptersController::gamify = ->
  videoid = "#" + $("#video-placeholder video").attr("id")
  player = videojs(videoid)
  limit = @params.limit
  events = new Picct.Models.GamingEvents(@params.events)

  checkEventLimit = ->
    if events.length >= limit
      $(".event-item").addClass("disabled")
      unless $("#event-list li.tetx-muted").length
        $("<li class='text-center text-muted'>#{t('course_chapters.gamify.event_limit_reached', limit: limit)}<div class='top-1'></div></li>")
          .prependTo("#event-list")
    else
      $(".event-item").removeClass("disabled")
      $("#event-list li.text-muted").remove()

  events.on "add", checkEventLimit
  events.on "remove", checkEventLimit
  events.on "reset", checkEventLimit

  checkEventLimit()

  updateIconVisibility = (event) ->
    currentTime = player.currentTime()
    if parseInt(event.get("start")) <= currentTime &&
        (parseInt(event.get("duration") + parseInt(event.get("start"))) >= currentTime)
      event.trigger("show:icon")
    else
      event.trigger("hide:icon")

  player.on "timeupdate", (e) ->
    events.each (event) -> updateIconVisibility(event)

  links.Timeline::onDblClick = (e) ->
    links.Timeline.preventDefault(e)
    false

  initSlider = (max) ->
    $("#time-slider").slider(
      max: max
      value: 0
      formater: (val) ->
        mins = Math.floor(parseInt(val) / 60)
        secs = parseInt(val) - mins*60
        mins + (if secs >= 10 then ":" else ":0") + secs
    ).on "slide", (e) ->
      player.currentTime($(@).slider("getValue"))
      timeline.setCustomTime($(@).slider("getValue"))

  initTimeline = (max) ->
    window.timeline = new links.Timeline($("#timeline")[0])

    links.events.addListener timeline, "ready", (e) ->
      timeline.draw [],
        start: 0
        min: 0
        end: max
        max: max
        autoResize: true
        showMajorLabels: false
        showMinorLabels: false
        showCustomTime: true
        editable: true
        zoomMax: max
      timeline.setCustomTime($("#time-slider").slider("getValue"))

    getModel = ->
      selection = timeline.getSelection()
      if selection.length && selection[0].row isnt undefined
        return events.get(timeline.getItem(selection[0].row).cid)

    links.events.addListener timeline, "timechange", (e) ->
      value = e.time.getMilliseconds()
      $("#time-slider").slider("setValue", value)
      player.currentTime(value)

    links.events.addListener timeline, "select", ->
      model = getModel()
      model.trigger("select") if model?

    links.events.addListener timeline, "delete", ->
      model = getModel()
      if model?
        model.fireDestroyCallback = false
        model.destroy()
        model.fireDestroyCallback = undefined

    # use this one since 'change' never fires when resizing items
    links.events.addListener timeline, "changed", ->
      model = getModel()
      if model?
        selectionIdx = timeline.getSelection()[0].row
        item = timeline.getItem(selectionIdx)

        if item.start.getHours() < item.end.getHours() # dragged to the left past second 0
          timeline.changeItem(selectionIdx, start: 0, end: parseInt(model.get("duration")))
          model.set(start: 0)
        else
          videoDuration = Math.floor(player.duration())
          eventDuration = item.end.getMilliseconds() - item.start.getMilliseconds()

          if item.end.getMilliseconds() > videoDuration # dragged past the end of the video
            timeline.changeItem(selectionIdx, start: videoDuration - eventDuration, end: videoDuration)
            model.set(start: videoDuration - eventDuration)
          else
            model.set
              start: item.start.getMilliseconds(),
              duration: item.end.getMilliseconds() - item.start.getMilliseconds()

  initControls = (max) ->
    initSlider(max)
    initTimeline(max)

    links.events.addListener timeline, "ready", (e) ->
      events.each (event) ->
        initEvent(event)
        updateIconVisibility(event)

  addTimelineEvent = (event) ->
    timeline.addItem(
      _.extend(_.clone(event.attributes),
        content: event.get("text") ||
          "<span class='text-muted'>#{t('course_chapters.gamify.timeline.placeholder')}</span>",
        cid: event.cid,
        end: parseInt(event.get("start")) + parseInt(event.get("duration"))))
    setTimeout (-> timeline.checkResize()), 500 # when exactly to trigger this ?

  getTimelineEventIndex = (cid) ->
    tdata = timeline.getData()
    _.indexOf(tdata, _.find(tdata, (item) -> item.cid == cid))

  changeTimelineEvent = (cid, properties) ->
    timeline.changeItem(getTimelineEventIndex(cid), properties)

  initEvent = (event) ->
    events.add(event)

    new Picct.Views.GamingEvents.Edit(model: event)
    new Picct.Views.GamingEvents.Icon(model: event, container: "#video-placeholder .event-canvas")

    addTimelineEvent(event)
    timeline.setSelection([row: timeline.getData().length - 1])
    event.trigger("select")

    event.on "change:start", ->
      changeTimelineEvent(@cid, end: parseInt(@get("start")) + parseInt(@get("duration")))
      updateIconVisibility(@)

    event.on "change:duration", ->
      changeTimelineEvent(@cid, end: parseInt(@get("start")) + parseInt(@get("duration")))
      updateIconVisibility(@)

    event.on "change:text", ->
      text = @get("text")
      unless text == ""
        changeTimelineEvent(@cid, content: text.substring(0,20))

    event.on "select", ->
      timeline.setSelection [ row: getTimelineEventIndex(@cid) ]

    event.on "destroy", ->
      timeline.deleteItem(getTimelineEventIndex(@cid)) unless @fireDestroyCallback is false

  createEvent = (params) ->
    params.start ?= player.currentTime()
    initEvent(new Picct.Models.GamingEvent(params))

  if $(videoid)[0].duration
    initControls($(videoid)[0].duration)
  else
    player.on "loadedmetadata", ->
      initControls(@duration())

  $(".view-trigger").click (e) ->
    timeline.setSelection []
    $("#form-tabs > *").hide()
    $($(@).data("target")).show()
    e.preventDefault()
    false

  $("#event-list").on "click", ".event-item:not(.disabled)", (e) ->
    createEvent(top: 0, left: 0)
    e.preventDefault()
    false

  $("#event-list").on "dragstart", ".event-item:not(.disabled)", (e) ->
    img = document.createElement("div")
    img.className = "popcorn-footnote"
    img.style.position = "absolute"
    img.style.top = "-100px"
    img.style.left = "-100px"
    document.body.appendChild(img)

    e.originalEvent.dataTransfer.setDragImage(img, 0, 0)
    e.originalEvent.dataTransfer.effectAllowed = "link"

  $(".event-canvas").on "dragover", (e) ->
    e.preventDefault()

  $(".event-canvas").on "drop", (e) ->
    e.stopPropagation()
    dim = $('<div class="popcorn-footnote">').height()
    top = e.originalEvent.pageY - $(e.target).offset().top - dim/2
    left = e.originalEvent.pageX - $(e.target).offset().left - dim/2

    if top < 0
      top = 0
    else if top + dim > e.target.offsetHeight
      top = e.target.offsetHeight - dim

    if left < 0
      left = 0
    else if left + dim > e.target.offsetWidth
      left = e.target.offsetWidth - dim

    top = Math.floor(top / e.target.offsetHeight * 100)
    left = Math.floor(left / e.target.offsetWidth * 100)

    if e.originalEvent.dataTransfer.dropEffect == "link"
      createEvent(top: top, left: left)
    else
      view = $(".dragging").removeClass("dragging").data("view")
      view.model.set(top: top, left: left) if view?

    false

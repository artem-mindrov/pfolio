UsersController = Paloma.controller("Admin/Users")

UsersController::index = ->
  submitForm = ->
    form = $("#admin-users-filter")
    $.get(form.attr("action"), form.serialize(), null, 'script')
    false

  $("#main").on "keyup", "#email-search", (e) ->
    _.debounce(submitForm(), 600)

  $("#main").on "change", "#admin-users-filter input[type=checkbox]", (e) ->
    submitForm()

  $("#main").on "click", "#users-list a.submit", ->
    tr = $(@).find("tr")
    tr.loadingOverlay()
    $.getScript @href, done: -> tr.loadingOverlay("remove")
    false

  $("#main").on "submit", "#admin-users-filter", ->
    $.get(@action, $(@).serialize(), null, 'script')
    false
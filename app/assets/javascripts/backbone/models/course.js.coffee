class Picct.Models.Course extends Picct.Models.ModelBase
  urlRoot: "/courses"

  associations: ->
    course_chapters: Picct.Models.CourseChapters

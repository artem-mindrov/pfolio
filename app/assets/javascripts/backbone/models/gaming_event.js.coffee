class Picct.Models.GamingEvent extends Picct.Models.ModelBase
  defaults: ->
    start: 0
    duration: 2
    top: 0
    left: 0

class Picct.Models.GamingEvents extends Picct.Models.CollectionBase
  url: "/gaming_events"
  model: Picct.Models.GamingEvent

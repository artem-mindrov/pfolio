class Picct.Models.CourseChapter extends Picct.Models.ModelBase
  associations: ->
    course: Picct.Models.Course

class Picct.Models.CourseChapters extends Picct.Models.CollectionBase
  model: Picct.Models.CourseChapter
  associations: ->
    course: Picct.Models.Course

  url: -> "/courses/#{@course.id}/course_chapters"

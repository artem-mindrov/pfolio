class Picct.Views.FormView extends Picct.Views.ViewBase
  events: ->
    "submit form": "submitForm"
    "jqv.form.result form": "handleValidation"

  clearErrors: ->
    @$(".form-group").removeClass("has-error")
    @$('label.error span.error').remove()
    @$('label.error, :input.error').removeClass('error')

  displayError: (key, message) ->
    input = @$("[name=#{key}]")

    if input.is(":visible")
      label = @$("label[for=#{key}]")
      label.closest(".form-group").addClass('has-error')
      if message?
        label.append "<span class='error'>#{message}</span>"
      @$("[name=#{key}]").addClass 'error'
    else
      message = "#{key} #{message}" if key != "base"
      flash(error: message)

  displayErrors: (model, errors) ->
    _.each errors.responseJSON.errors, (failedArray, key) =>
      @displayError key, @errorMessageFor(model, key, _.first(failedArray))

  errorMessageFor: (model, key, validation) ->
    model.errorMessageFor key, validation

  render: ->
    super

    if @$(".select2-field").length
      @$("form").validationEngine
        validateNonVisibleFields: true
        prettySelect: true
        usePrefix: 's2id_'
        autoPositionUpdate: true
    else
      @$("form").validationEngine(validateNonVisibleFields: true)

    @editors = editables(@$(".editable"))

  submitForm: (event) ->
    event.preventDefault()
    @clearErrors()

    valid = if @hasErrors is undefined then false else (@hasErrors is false)

    if !valid && !@$("form:not(.validating)").validationEngine("validate")
      valid = false

    if valid
      @$el.loadingOverlay()
      formData = @getAttrs()
      attrs = ( _.find(_.values(formData), (v) -> _.isObject(v)) )
      options =
        error: @_error
        success: @_success
        data: JSON.stringify(formData)
        contentType: "application/json"

      if @model?
        @model.set(attrs)
        @model.save(null, options)
      else if @collection?
        model = @collection.create(attrs, options)

    false

  getAttrs: ->
    @clearErrors()
    Backbone.Syphon.serialize @

  handleValidation: (evt, error) ->
    if error
      @$("form :submit").removeAttr("disabled")
      @hasErrors = true
    else
      @hasErrors = false

  onError: (model, errors, options) ->
    @displayErrors model, errors

  _completed: (model, response) ->
    @$el.loadingOverlay("remove")
    $.rails.enableFormElements @$('form')

  _error: (model, errors, options) =>
    @onError model, errors, options
    @trigger 'form.fail', model, errors, options
    @trigger 'form.always', model, errors, options
    @_completed model, errors

  _success: (model, response) =>
    @trigger 'form.done', model, response
    @trigger 'form.always', model, response
    @_completed model, response

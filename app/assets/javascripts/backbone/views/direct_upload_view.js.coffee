class Picct.Views.DirectUploadView extends Picct.Views.FormView
  events: -> _.extend super,
    "click .pause":  "pause"
    "click .resume": "resume"
    "click .cancel": "cancel"

  render: ->
    super
    @initUploader()

  initUploader: ->
    uploader = @$el.findWithRoot(".direct-uploader")
    fileUploadOptions =
      uploadTemplateId: "video-upload-tmpl"
      acceptFileTypes: /^video\/.*/i
      dropZone: uploader.find(".dropzone")
    @model.uploader = new DirectUploader(uploader.data("fileupload-options", fileUploadOptions), model: @model)

    @model.uploader.on "upload:start",    @uploadStarted, @
    @model.uploader.on "upload:complete", @uploadCompleted, @
    @model.uploader.on "upload:pause",    @uploadPaused, @
    @model.uploader.on "upload:resume",   @uploadResumed, @
    @model.uploader.on "upload:cancel",   @uploadCancelled, @
    @model.uploader.on "upload:fail",     @uploadFailed, @
    @model.uploader.on "upload:progress", @uploadProgress, @

  getAttrs: ->
    @clearErrors()
    Backbone.Syphon.serialize(@$(".main-form form")[0])

  pause:  (e) -> e.preventDefault(); @model.uploader.pause()
  resume: (e) -> e.preventDefault(); @model.uploader.resume()
  cancel: (e) -> e.preventDefault(); @model.uploader.cancel()

  uploadStarted: -> {}
  uploadCompleted: -> {}
  uploadPaused: -> {}
  uploadResumed: -> {}
  uploadCancelled: -> {}
  uploadFailed: -> {}
  uploadProgress: -> {}
class Picct.Views.Courses.Edit extends Picct.Views.CourseUploadView
  events: -> _.extend super,
    "click .save-all": "saveAll"

  initialize: (options) ->
    super
    @modelBinder = new Backbone.ModelBinder()
    @render()

  render: ->
    @setElement("#direct-uploader")
    bindings = _.object(_.map(["title", "description"], (attr) -> [attr, "[name*='#{attr}']"]))
    bindings["fee"] =
      selector: "[name*='fee']"
      converter: (dir, val) -> parseInt(val) || 0

    @modelBinder.bind @model, @el, bindings

    super

    format = (item) ->
      "<p>#{item.name || item.email}</p>"

    author = @model.get("author")

    @$(".select2-field").select2
      minimumInputLength: 3
      formatResult: format
      formatSelection: format
      blurOnChange: true
      initSelection: author && (el, cb) -> cb(author)
      ajax:
        url: "/users"
        dataType: "json"
        data: (term, page) ->
          q: term
        results: (data, page) -> data

    setSelect2Hooks()

  _success: ->
    if @model.course_chapters?
      id = @model.id
      @model.course_chapters.each( (chapter) -> chapter.set("course_id", id) )

    super

  _completed: ->
    chapters = @model.course_chapters

    if chapters? && chapters.any( (chapter) -> chapter.dirty? )
      chapters.saveChained = true
      chapters.once "update:done", @chainedUpdateDone, @
      chapters.find( (chapter) -> chapter.dirty? ).trigger("submit")
    else
      @chainedUpdateDone()

    super

  saveAll: ->
    @$(".save-all").attr("disabled", "disabled")

  chainedUpdateDone: ->
    @$(".save-all").removeAttr("disabled")
    @model.course_chapters.saveChained = undefined if @model.course_chapters
    @$(".upload-control").hide()

  uploadingModels: ->
    @model.course_chapters.where( (model) -> model.uploading? )

  pause: ->
    super
    if @model.course_chapters
      _.each( @uploadingModels(), (model) -> model.uploader.pause())

  resume: ->
    super
    if @model.course_chapters
      _.each( @uploadingModels(), (model) -> model.uploader.resume())

  cancel: ->
    super
    if @model.course_chapters
      _.each( @uploadingModels(), (model) -> model.uploader.cancel())
class Picct.Views.GamingEvents.Icon extends Picct.Views.ViewBase
  events:
    "click":     "select"
    "dragstart": "startDragging"

  initialize: (options) ->
    super
    @container ?= options.container
    @model.on "change:top", => @$el.css(top: "#{@model.get('top')}%")
    @model.on "change:left", => @$el.css(left: "#{@model.get('left')}%")
    @model.on "show:icon", @show, @
    @model.on "hide:icon", @hide, @
    @model.on "destroy", @destroy, @
    @render()

  render: ->
    @setElement("<a href='#'><div class='popcorn-footnote'></div></a>")
    @$el.css(
      display: "block"
      position: "absolute"
      top: "#{@model.get('top')}%"
      left: "#{@model.get('left')}%"
    ).data("view", @).appendTo(@container)
    super

  select: (e) ->
    e.preventDefault()
    @model.trigger("select")
    false

  startDragging: (e) ->
    @$el.addClass("dragging")
    e.originalEvent.dataTransfer.effectAllowed = "move"

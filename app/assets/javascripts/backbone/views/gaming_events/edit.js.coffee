class Picct.Views.GamingEvents.Edit extends Picct.Views.FormView
  events: ->
    _.extend super, "click a.destroy": "invokeDestroy"

  initialize: ->
    super
    @model.on "select", @show, @
    @model.on "destroy", @destroy, @
    @modelBinder = new Backbone.ModelBinder()

  invokeDestroy: (e) ->
    @$el.loadingOverlay()
    @model.destroy()
    e.preventDefault()
    false

  render: ->
    # bindings are invoked twice to work around the issue with the placeholder
    # being displayed over non-empty contenteditable areas
    @$el.html($("#form-template-container").html()).appendTo($("#form-tabs"))
    bindings = _.object(_.map(["top", "left", "start", "duration", "text"],
      (attr) -> [attr, "[name*='#{attr}']"]))
    @modelBinder.bind @model, @el, bindings

    super

    @$el.on "keyup", "[name*='top'], [name*='left']", (e) ->
      val = parseInt($(@).val())
      if _.isNaN(val) || val < 0 || val > 100
        $(@).val(50)

    bindings["text"] =
      selector: ".gaming_event_text div.editable"
      elAttribute: "html"

    @modelBinder.bind @model, @el, bindings

  show: ->
    $("#form-tabs > *").hide()
    super

  destroy: ->
    super
    $("#form-tabs > #event-list").show()
class Picct.Views.CourseChapters.Sortable extends Picct.Views.ViewBase
  events: ->
    "click .save-order": "reorder"

  initialize: (options = {}) ->
    super
    @container ?= options.container
    @collection.on "add", @addChapter, @
    @collection.on "remove", @checkSaveOrder, @
    @render()

  render: ->
    @setElement($(@container))

    super

    if !@collection.length
      @$("em").css(opacity: 0)

    @$("> div.text-center").click =>
      @collection.add(new Picct.Models.CourseChapter(course: @collection.course))
      @$("em").fadeTo(200, 1)

    @el.addEventListener "dragover", (e) =>
      @$el.addClass("dragover")
      e.preventDefault()
      e.dataTransfer.dropEffect = 'copy'
      false

    @el.addEventListener "dragleave", (e) =>
      @$el.removeClass("dragover")

    @el.addEventListener "drop", (e) =>
      # somehow if files are dropped onto a video element, the event fires in the opposite order
      # so prevent handling it here
      if $(e.target).closest(".template-upload").length
        return false

      if e.dataTransfer && e.dataTransfer.files.length
        e.preventDefault()
        e.stopPropagation()
        @$el.removeClass("dragover")

        chapter = new Picct.Models.CourseChapter(course: @collection.course)
        @collection.add(chapter)
        @$("em").fadeTo(200, 1)

        chapter.trigger("drop:video", e)

    @$el.on "click", ".destroy", =>
      unless @collection.length
        @$("em").fadeTo(200, 0)

    @$el.findWithRoot(".sortable").sortable(
      handle: ".sortable-handle"
      placeholder: "<div class='panel-heading panel-title sortable-placeholder'></div>"
      forcePlaceholderSize: true
    ).bind "sortupdate", (e,drag) =>
      step = 32768
      prev = drag.item.prev().find("input[name$='position]']")
      next = drag.item.next().find("input[name$='position]']")
      prevPos = BigInteger(prev.val())
      nextPos = BigInteger(next.val())
      curPos = BigInteger(drag.item.find("input[name$='position]']").val())
      orderChanged = false

      if drag.item.hasClass("panel-success")
        prevPersisted = drag.item.prevAll("div.panel-success:first").find("input[name$='position]']")
        nextPersisted = drag.item.nextAll("div.panel-success:first").find("input[name$='position]']")

        if nextPersisted.length == 0 # moved to the end
          if BigInteger(prevPersisted.val()).compare(curPos) != -1
            orderChanged = true
        else if prevPersisted.length == 0 # moved to the head
          if curPos.compare(BigInteger(nextPersisted.val())) != -1
            orderChanged = true
        else # somewhere in between
          unless BigInteger(prevPersisted.val()).compare(curPos) == -1 &&
              curPos.compare(BigInteger(nextPersisted.val())) == -1
            orderChanged = true

      if next.length == 0 # must have been moved to the end of the list
        drag.item.find("input[name$='position]']").xVal(prevPos.add(step))
      else
        if nextPos.subtract(prevPos).compareAbs(1) <= 0
          @resetPositions(step)
          orderChanged = true
        else
          drag.item.find("input[name$='position]']").xVal(nextPos.add(prevPos).divide(2).toString())

      if orderChanged
        if @collection.any( (model) -> model.dirty? ) # unsaved items
          @collection.reordered = true
        else
          @$(".save-order").fadeIn(1000)

  resetPositions: (step) ->
    curPos = BigInteger(0)

    @$("input[name$='position]']").each ->
      $(@).xVal(curPos.toString())
      curPos = curPos.add(step)

  addChapter: (chapter) ->
    new Picct.Views.CourseChapters.Edit(model: chapter)
    @$el.findWithRoot(".sortable").sortable("reload")

  checkSaveOrder: ->
    if @collection.reordered? # check if sort order changed but saving was not allowed
      if @collection.any( (model) -> model.dirty? ) # check if any unsaved items remain
        @$(".save-order").fadeIn(1000)
        @collection.reordered = undefined

  reorder: (e) ->
    e.preventDefault()
    e.stopPropagation()
    course_id = @collection.first().get("course_id")

    if course_id
      @$(".save-order").text(t("saving")).attr("disabled", "disabled")
      order = _.object( @collection.map, (model) -> [ model.id, model.get("position") ] )

      $.ajax
        url: "/courses/#{course_id}/reorder"
        type: "POST"
        data: { order: order }
        dataType: "script"
        complete: =>
          @$(".save-order").fadeOut(1000).text(t("save_order")).removeAttr("disabled")

class Picct.Views.CourseChapters.Edit extends Picct.Views.CourseUploadView
  events: -> _.extend super,
    "drop .dropzone": "handleDrop"
    "click .panel-heading": "toggleForm"
    "click a.update": "updateChapter"
    "click a.destroy": "destroyChapter"

  initialize: (options) ->
    super

    @modelBinder = new Backbone.ModelBinder()

    @model.on "submit", @updateChapter, @
    @model.on "change", @enableUpdate, @
    @model.on "drop:video", @dropVideo, @
    @model.on "destroy", @destroy, @

    @render()

  render: ->
    if @model.isNew()
      @setElement $($("#new-chapter-container").html()).appendTo($("#chapter-container"))
    else
      @setElement $("#course_chapter_#{@model.id}").closest(".chapter-item")

    super

    @$(".panel-collapse").collapse()

    bindings = _.object(_.map(["position", "description", "course_id"],
      (attr) -> [attr, "[name*='#{attr}']"]))
    bindings["title"] = [
      {selector: "[name*='title']"},
      {selector: ".panel-heading span.heading"}
    ]
    @modelBinder.bind @model, @el, bindings

    @$(".upload-control").hide()
    @$(".main-form form :submit").hide()

    @uploaderForm = @$(".uploader-form form")
    @mainForm = @$(".main-form form")

    @disableUpdate()
    @uploaderForm.on "fileuploadadd", =>
      @enableUpdate()
      @model.trigger("change:video")

    @mainForm.validationEngine(validateNonVisibleFields: true)
    @uploaderForm.validationEngine(validateNonVisibleFields: true)

  handleValidation: (evt, error) ->
    super
    if error
      @$el.addClass("panel-danger").removeClass("panel-success panel-info")
      @updateNext()

  toggleForm: (e) ->
    if !$(e.target).is("i")
      @$(".panel-collapse").collapse("toggle")

  updateChapter: (e) ->
    if e isnt undefined
      e.preventDefault()
      e.stopPropagation()

    if @model.dirty?
      @$el.removeClass("panel-danger panel-success panel-info")
      @mainForm.find(":submit").trigger("click")

    false

  destroyChapter: (e) ->
    e.preventDefault()
    e.stopPropagation()

    @model.destroy()
    false

  hide: ->
    @$el.fadeOut(500)
    @

  enableUpdate: ->
    @model.dirty = true
    @$("a.update").show() unless @model.course && @model.course.isNew()

  disableUpdate: ->
    @model.dirty = undefined
    @$("a.update").hide()

  handleDrop: (e) -> e.stopPropagation()

  dropVideo: (e) ->
    @$(".direct-uploader .dropzone").trigger($.Event(e)).removeClass("in")

  _success: (model, response) =>
    super
    @$el.removeClass("panel-danger").addClass("panel-success")
    model.collection.trigger("order:check") if model.collection?

  _error: (model, errors, options) =>
    super
    @$el.removeClass("panel-info panel-success").addClass("panel-danger")

  _completed: (model, response) ->
    super
    @updateNext()

  updateNext: (model) ->
    @disableUpdate()
    chapters = @model.collection

    if chapters? && chapters.saveChained?
      next = chapters.find (model) -> model.dirty?

      if next?
        next.trigger("submit")
      else
        chapters.trigger("update:done")
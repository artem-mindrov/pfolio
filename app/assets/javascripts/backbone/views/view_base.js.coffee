class Picct.Views.ViewBase extends Backbone.View
  initialize: (options) ->
    @template ?= options.template

  destroy: ->
    @hide().unbind().remove()
    @

  events: ->
    {}

  hide: ->
    @$el.hide()
    @

  remove: ->
    @$el.remove()
    @

  html: (html) ->
    @$el.html html
    @

  render: ->
    @_rendered = true

  show: ->
    @render() unless @_rendered?
    @$el.show()
    @

  unbind: ->
    @undelegateEvents()
    @model.off(null, null, @) if @model
    @collection.off(null, null, @) if @collection
    @
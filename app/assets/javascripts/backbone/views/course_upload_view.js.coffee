class Picct.Views.CourseUploadView extends Picct.Views.DirectUploadView
  events: -> _.extend super,
    "click .dropzone": "clickDropzone"

  render: ->
    super
    @$(".upload-control").hide()

  uploadStarted: (upload) ->
    filename = @$(".template-upload").data("data").files[0].name
    $("#upload-status").fadeIn(500).find("span").text(t("uploading", filename: filename))
    @$(".upload-control.pause, .upload-control.cancel").show()
    @model.uploading = true

  uploadCompleted: (upload) ->
    @model.uploading = undefined
    if upload isnt undefined
      @$(".progress-bar").css width: "100%"
    $("#upload-status").text(t("upload_complete")) if $(".remote-upload").length == 0

  uploadResumed: (key) ->
    @$(".upload-control.pause").show()
    @$(".upload-control.resume").hide()

  uploadPaused: (key) ->
    @$(".upload-control.pause").hide()
    @$(".upload-control.resume").show()

  uploadCancelled: (key) ->
    @uploadCompleted()
    @$(".upload-control").hide()
    $("#upload-status").fadeOut(500)

  uploadFailed: (err) ->
    @uploadCompleted()
    @$(".upload-control.pause, .upload-control.cancel").hide()
    $("#upload-status").fadeOut(500)

  clickDropzone: (e) ->
    if $(e.target).hasClass("dropzone") or $(e.target).hasClass("image-preview")
      @$(":file").trigger('click')

#= require_self
#= require ./model
#= require_tree ./models
#= require ./views/views

window.Picct =
  Models: {}
  Views:
    Courses: {}
    CourseChapters: {}
    GamingEvents: {}

Picct.Models.Associations =
  setAssociations: (options) ->
    if options?
      _.each _.result(@, "associations"), (value, key) =>
        if options[key]
          if options[key] instanceof value
            @[key] = options[key]
          else
            @[key] = new value(options[key])

class Picct.Models.ModelBase extends Backbone.Model
  _.extend(@prototype, Picct.Models.Associations)

  initialize: (options) ->
    super
    @setAssociations(options)

  errorMessageFor: (key, validation) ->
    if @errors?[key]? && @errors[key][validation]?
      @errors[key][validation]

class Picct.Models.CollectionBase extends Backbone.Collection
  _.extend(@prototype, Picct.Models.Associations)

  initialize: (options) ->
    super
    @setAssociations(options)


$.fn.setTypeForHook = (type) ->
  @each -> @type = type
  @

$.fn.xVal = ->
  result = $(@).val.apply(@, arguments)
  result.change() if result instanceof jQuery
  result

$.fn.findWithRoot = (selector) ->
  @filter(selector).add(@find(selector))

$.fn.loadingOverlay.defaults.loadingText = ""

window.setSelect2Hooks = ->
  $(".select2-field").select2('container').setTypeForHook('select2')

  $.valHooks['select2'] =
    get: (el) -> $(el).select2("val")
    set: (el, val) -> $(el).select2("val", val)

window.editable = (element) ->
  $this = $(element)
  styles = $this.attr('style')
  placeholder = $this.attr('placeholder')
  attrs = ""

  if styles isnt undefined
    attrs += "style='#{styles}' "

  if $this.attr('placeholder') isnt undefined
    attrs += "data-placeholder='#{placeholder}'"

  $this.wrap('<div class="editable-wrapper"/>').parent().prepend("<div class='editable' #{attrs}></div>")
  wrapper = $this.closest('.editable-wrapper')
  editable = wrapper.find("div.editable")
  editable.html($this.val())
  $this.hide().data("jqv-prompt-at", wrapper)

  $this.closest("form").submit -> $this.val(editable.html())

  new MediumEditor(editable)

window.editables = (elements) ->
  _.map elements, (element) -> editable(element)

window.t = (key) ->
  return "N/A" unless key

  keys = key.split(".")
  args = arguments[1]
  comp = window.I18n

  $(keys).each (key, value) -> comp = comp[value] if comp

  if !(typeof comp is "string") && _.has(args, "count")
    switch args["count"]
      when 0 then comp = comp["zero"]
      when 1 then comp = comp["one"]
      else comp = comp["other"]

  if !comp
    console.log("No translation found for key: #{key}")
    return "N/A"

  for arg of args
    rx = ///
      %{#{arg}}
        ///g
    comp = comp.replace(rx, args[arg])

  comp

window.formatFileSize = (bytes) ->
  if typeof bytes isnt 'number'
    return ''
  if bytes >= 1000000000
    return (bytes / 1000000000).toFixed(2) + ' GB'
  if (bytes >= 1000000)
    return (bytes / 1000000).toFixed(2) + ' MB'
  return (bytes / 1000).toFixed(2) + ' KB'

window.loadAjaxContent = (from) ->
  target = $(from.data('ajax-target'))
  url = from.data('source')
  target.loadingOverlay()

  $.ajax
    url: url
    complete: (data, status, xhr) ->
      target.loadingOverlay("remove").html(data.responseText)

window.flash = (flash = {}) ->
  classes =
    notice: "alert alert-success"
    error: "alert alert-danger"
    warning: "alert alert-warning"

  html = ""

  for type of flash
    html += "<div class=\"#{classes[type]} alert-dismissable\">" +
      "<a class=\"close\" data-dismiss=\"alert\"><i class='fa fa-times'></i></a>" +
      "<div id=\"flash_#{type}\">#{flash[type]}</div></div>"

  $('#flash').html(html)

window.showFlashHeaders = (xhr) ->
  f = $.parseJSON(xhr.getResponseHeader('X-Flash-Messages'))
  return if !f or $.isEmptyObject(f)
  flash(f)

$(document).ajaxComplete (e, xhr, options) ->
  showFlashHeaders(xhr)

$ ->
  $("#main").on "click", ".remote", (e) ->
    loadAjaxContent($(@))

  $("a.tab-remote").on "show", (e) ->
    target = $(e.relatedTarget).data('ajax-target')
    $(target).empty()
    loadAjaxContent($(@))

  $("ul.nav-tabs li.active a.tab-remote[data-toggle=tab]").each ->
    loadAjaxContent($(@))

  $(".scrolltofixed").scrollToFixed()

  $("#main").on "click", ".dropdown.toggle-caption li a", ->
    $(@).closest(".dropdown").find("a[data-toggle=dropdown]").text($(@).text())

  $(".modal").on "hide.bs.modal", ->
    $(@).find("video").each (i) ->
      @pause()
      @currentTime = 0

  $.validationEngine.defaults.promptPosition = "topLeft"

  $("form").on "jqv.form.result", ->
    $(@).find(".formError").each ->
      $(@).css(marginTop: -$(@).height())

  $("div,input,select,textarea").on "jqv.field.result", ->
    prompt = $(@).prev(".formError")
    prompt.css(marginTop: -prompt.height())

  $(document).on 'nested:fieldAdded', (event) ->
    $(event.field).validationEngine()

  $(document).on 'nested:fieldRemoved', (event) ->
    $(event.field).validationEngine("detach")
    event.field.find("input,select,textarea").removeClass (i,css) ->
      (css.match(/validate\S*/g) || []).join(" ")

  $(document).on 'dragover', (e) ->
    dropZone = $('.dropzone')
    timeout = window.dropZoneTimeout

    unless timeout
      dropZone.addClass('in')
    else
      clearTimeout(timeout)

    found = false
    node = e.target

    loop
      for i in dropZone
        if node is dropZone[i]
          found = true; break
      break if found

      node = node.parentNode
      break if node == null

    if found
      dropZone.addClass('hover')
    else
      dropZone.removeClass('hover')

    window.dropZoneTimeout = setTimeout ->
      window.dropZoneTimeout = null
      dropZone.removeClass('in hover')
    , 100

  $('body').on 'click', (e) ->
    $('[data-toggle="popover"], [rel="popover"]').each ->
      if (!$(@).is(e.target) && $(@).has(e.target).length == 0 && $('.popover').has(e.target).length == 0)
        $(@).popover('hide')

  $("#main").on 'ajax:beforeSend', ".pagination a", ->
    $(@).closest("[data-overlay]").loadingOverlay()
    history.pushState {}, '', @href


class DirectUploader
  _.extend(@prototype, Backbone.Events)

  constructor: (container, attrs) ->
    _.extend(@, attrs)
    @container = $(container)
    @mainForm = @container.find(".main-form form:first")
    @uploaderForm = @container.find(".uploader-form form:first")
    @saving = undefined
    @fileuploadData = undefined
    @fileAdded = false
    @dropZoneEnabled = !!@uploaderForm.findWithRoot(".dropzone").length
    @options =
      autoUpload: false
      replaceFileInput: false
      uploadTemplateId: null
      downloadTemplateId: null
      add: $.proxy(@add, @)
      dataType: "xml"

    unless window.production
      @options = $.extend @options,
        progress: $.proxy(@progress, @)
        done: $.proxy(@done, @)
        fail: $.proxy(@fail, @)
        dataType: "json"

    @options = $.extend(@options, (@container.data("fileupload-options") || {}))
    @options.templatesContainer ?= @uploaderForm.find(".image-preview") if @dropZoneEnabled
    @initFileUploader @options
    @setupEvents()

  add: (e, data) ->
    @fileuploadData = data
    @fileAdded = true

    if @dropZoneEnabled
      file = data.files[0]
      form = @uploaderForm
      fu = form.data("blueimp-fileupload") || form.data("fileupload")

      form.fileupload('process', data).done ->
        # see _hasError() in jquery-fileupload-ui
        if @options.acceptFileTypes
          unless @options.acceptFileTypes.test(file.type) or @options.acceptFileTypes.test(file.name)
            file.error = t("fileupload.errors.type_not_allowed")

        if @options.maxFileSize and @options.maxFileSize < file.size
          file.error = t("fileupload.errors.file_too_big", size: formatFileSize(@options.maxFileSize))

        data.files.valid = data.isValidated = if file.error then false else true
        result = tmpl(@options.uploadTemplateId, file)
        data.context = $(@options.templatesContainer).html(result).children().data("data", data)

        fu._renderPreviews(data)
        fu._transition(data.context)

        form.find(".image-preview").html(data.context)
        form.find(":file").validationEngine("validate")

  appendKey: (form, key) ->
    input = $("<input>").attr(
      type: "hidden"
      name: @uploaderForm.data("model") + "_key"
    ).val(key)
    input.appendTo form

  commitEvent: (e) ->
    e.preventDefault()

    if @uploaderForm.validationEngine("validate") && @mainForm.validationEngine("validate")
      return @submitMainForm() unless @fileAdded

      uploader = @
      fileuploadData = @fileuploadData
      file = @fileuploadData.files[0]
      return if @saving

      @saving = true

      if file isnt undefined
        @uploaderForm.find(":file").attr "disabled", "disabled"

        unless @dropZoneEnabled
          @fileuploadData.context = $(tmpl(@options.uploadTemplateId, file).trim())
          @uploaderForm.append @fileuploadData.context

        unless window.production
          @s3mp = new S3MPDummy()
          @trigger("upload:start", @fileuploadData)
          @container.loadingOverlay()
          @fileuploadData.submit()
            .done( =>
              @trigger("upload:complete")
              @submitMainForm())
            .fail( => @trigger("upload:fail"))
            .always( => @container.loadingOverlay("remove"))
        else
          @container.loadingOverlay()
          @s3mp = new window.S3MP
            bucket: window.s3_bucket
            fileInputElement: @uploaderForm.find(":file")
            fileList: [ file ]
            headers:
              acl: "public-read"

            onStart: (upload) =>
              @container.loadingOverlay("remove")
              @key = upload.key
              @appendKey @mainForm, upload.object_name
              @submitMainForm() if @mainForm.data("remote")
              @trigger("upload:start", upload)

            onComplete: (upload) =>
              @submitMainForm() unless @mainForm.data("remote")
              @trigger("upload:complete", upload)

            onPause: (key) =>
              @trigger("upload:pause", key)

            onResume: (key) =>
              @trigger("upload:resume", key)

            onCancel: (key) =>
              @saving = undefined
              fileuploadData.context.find(".progress-bar").css "width", "0%"
              @trigger("upload:cancel", key)

            onError: (err) =>
              @saving = undefined
              @trigger("upload:fail", err)

              if err.message
                if err.name == "FileSizeError" # FileSizeError in s3_multipart/lib.js
                  flash(error: t("s3_multipart.errors.limits.min", min: formatFileSize(5*1000*1000)))
                else if err.name = "ServerResponse"
                  flash(error: err.message)
              else
                flash(err)

            onProgress: (num, size, done, percent, speed) =>
              fileuploadData.context.find(".progress-bar").css "width", percent + "%"
              @trigger("upload:progress", num, size, done, percent, speed)
      else
        @saving = undefined
        alert "#{file.name} #{t('fileupload.errors.format_incorrect')}"

  initFileUploader: (options) ->
    @uploaderForm.fileupload(options)

  setupEvents: ->
    submit = @container.find(".main-submit")
    submit = @mainForm.find(":submit") unless submit.length
    submit.click $.proxy(@commitEvent, @)

  submitMainForm: ->
    @mainForm.submit()

  done: (e, data) ->
    @trigger("upload:complete", data)

  fail: (e, data) ->
    @saving = `undefined`
    @trigger("upload:fail", data.files[0].error)

  progress: (e, data) ->
    percent = parseInt(data.loaded / data.total * 100, 10)
    @fileuploadData.context.find(".progress-bar").css "width", percent + "%"
    @trigger("upload:progress", 0, 0, 0, percent, 0)

  pause: -> @s3mp.pause(@key)
  resume: -> @s3mp.resume(@key)
  cancel: -> @s3mp.cancel(@key)

window.DirectUploader = DirectUploader

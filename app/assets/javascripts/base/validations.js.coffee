window.dropzoneEmpty = (field,rules,i,options) ->
  data = field.closest("form").find(".template-upload").data("data")

  unless data && data.files && data.files.length
    return true

window.validateDuration = (field,rules,i,options) ->
  video = field.closest(".direct-uploader").find("video")[0]

  if video
    if video.duration && video.duration > 300 # 5 minutes max
      return options.allrules.video.alertTextDurationExceeded
    else
      video.addEventListener "loadedmetadata", ->
        field.validationEngine("validate")

window.editableEmpty = (field,rules,i,options) ->
  return true if field.closest(".editable-wrapper").find("div.editable").html() == ""
module ApplicationHelper
  def resource_name
    :user
  end

  def resource
    @resource ||= User.new
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end

  def humanize_price_range(range)
    pt = range.price_tags
    if pt.first.price_cents == pt.last.price_cents
      humanize_money_with_symbol(pt.first.price)
    else
      humanize_money_with_symbol(pt.first.price) + " - " + humanize_money_with_symbol(pt.last.price)
    end
  end

  def multipart_upload_input(options = { input_name: "s3_uploader", types: ["video"] })
    file_field_tag options[:input_name],
                   accept: options[:types].join(','),
                   multiple: ('multiple' if options.key?(:multiple)),
                   data: {uploader: S3Multipart::Uploader.serialize(options[:uploader])}
  end

  def i18n_js
    b = I18n.backend

    @translations ||= b.send(:translations)[I18n.locale] || begin
      b.send(:load_file, Rails.root.join("config", "locales", "#{I18n.locale}.yml"))
      b.send(:translations)[I18n.locale]
    end

    javascript_tag do
      "window.I18n = #{@translations.with_indifferent_access.to_json}".html_safe
    end
  end

  # Forms a `<script>` tag setting JavaScript variable `window.Mobvious.device_type`
  # to a string holding current request's device type. You can then use
  # `Mobvious.device_type` in all your JavaScripts to get this value.
  #
  # Example:
  #
  # <head>
  # <%= mobvious_javascript %>
  #
  # <script type="text/javascript">
  # alert(Mobvious.device_type); // this will print 'mobile' or 'desktop' or similar
  # </script>
  # </head>
  #
  # @return [String] script tag that sets Mobvious variables (returned string is html_safe)
  def mobvious_javascript
    <<-END.html_safe
      <script type="text/javascript">
        if (window.Mobvious === undefined) {
          window.Mobvious = {};
        }
        window.Mobvious.device_type = '#{device_type.to_s}';
      </script>
    END
  end
end

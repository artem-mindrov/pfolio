module CarrierWave
  module Delayed
    module Processing
      if Rails.env.development?
        extend ActiveSupport::Concern

        included do
          self.uploaders.each do |column, uploader|
            self.class_eval <<-CODE, __FILE__, __LINE__+1
              def #{column}_with_delay=(url)
                CarrierWave::Delayed::Worker.new.async.perform(self, :#{column}, url)
              end
              alias_method_chain :#{column}=, :delay
            CODE
          end
        end
      end
    end

    class Worker
      include SuckerPunch::Job

      def perform(model, method, url)
        model.send(:"#{method}_without_delay=", url)
        model.save
      end
    end
  end
end
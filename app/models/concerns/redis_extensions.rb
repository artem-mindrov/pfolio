class Redis
  module Extensions
    extend ActiveSupport::Concern

    included do
      after_destroy :delete_redis_keys
    end

    def delete_redis_keys
      self.class.redis_objects.each do |key,opts|
        redis.del(redis_field_key(key))
      end
    end
  end
end
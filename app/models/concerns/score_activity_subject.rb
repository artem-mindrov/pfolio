module ScoreActivitySubject
  extend ActiveSupport::Concern

  included do
    has_many :score_activities, as: :subject, dependent: :destroy
  end
end
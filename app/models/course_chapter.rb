require 'delayed_processing'
require 'score_activity_subject'

class CourseChapter < ActiveRecord::Base
  EVENT_LIMIT = 3
  mount_uploader :video, VideoClipUploader
  include CarrierWave::Delayed::Processing
  include ScoreActivitySubject

  belongs_to :course
  belongs_to :s3_multipart_upload, class_name: "S3Multipart::Upload"
  has_many :gaming_events, dependent: :destroy

  validates :title, presence: true
  validates :video, is_uploaded: true unless Rails.env.development?

  default_scope { order("position asc") }

  def to_s
    self.title
  end

  def next
    self.course.course_chapters.where("position >= ? and id != ?", position, id).first
  end

  def previous
    self.course.course_chapters.where("position <= ? and id != ?", position, id).last
  end

  def save_and_process(options = {})
    if options[:now]
      logger.info "Processing chapter #{self.id}..."
      self.remote_video_url = video.url
      self.processing = false
      save
    else
      update_attribute(:processing, true)
      logger.info "Queueing chapter #{self.id}..."
      CourseProcessor.new.async.perform(attributes.merge(model: self.class.name))
    end
  end
end

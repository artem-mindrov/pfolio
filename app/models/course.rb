require 'delayed_processing'
require 'redis_extensions'
require 'score_activity_subject'

class Course < ActiveRecord::Base
  LEVELS = %w{ beginner intermediate pro }.map { |key| I18n.t("activerecord.attributes.course.level_values.#{key}") }
  mount_uploader :intro, VideoClipUploader
  include CarrierWave::Delayed::Processing
  include Redis::Objects
  include Redis::Extensions
  include ScoreActivitySubject

  extend FriendlyId
  friendly_id :title, use: [:slugged, :finders]

  belongs_to :author, class_name: "User"
  belongs_to :s3_multipart_upload, class_name: "S3Multipart::Upload"
  has_many :course_chapters, dependent: :destroy
  has_many :enrolments, dependent: :destroy
  has_many :students, through: :enrolments, source: :user

  scope :viewable, -> { joins(:course_chapters).
      where(%q{courses.processing is not true and not exists
        (select * from course_chapters
          where course_chapters.processing = ? and course_chapters.course_id = courses.id)}, true).uniq
  }

  validates :title, :author_id, presence: true
  validates :fee, numericality: true, allow_blank: true

  attr_accessor :newly_created
  set :anonymous_views
  set :user_views

  paginates_per 18

  def save_and_process(options = {})
    if options[:now]
      logger.info "Processing course #{self.id}..."
      self.remote_intro_url = intro.url
      self.processing = false
      save
    else
      update_attribute(:processing, true)
      logger.info "Queueing course #{self.id}..."
      CourseProcessor.new.async.perform(attributes.merge(model: self.class.name))
    end
  end

  def to_s
    self.title
  end

  def reorder(params = {})
    status, msg = true, ""

    self.transaction do
      params.each do |chapter_id, position|
        chapter = self.course_chapters.where(id: chapter_id).first

        if chapter.nil?
          status, msg = false, "Chapter #{chapter_id} is not a member of course #{self.id}"
          break
        end

        unless chapter.update_attribute(:position, position)
          status, msg = false, "Chapter #{chapter_id} failed to save: #{self.error.full_messages.join(", ")}"
          break
        end
      end
    end

    logger.error msg unless status
    [status, msg]
  end

  def unique_views
    anonymous_views.count + user_views.count
  end
end

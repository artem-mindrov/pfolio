class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new

    if user.has_role?(:admin)
      can([:read, :create, :update, :destroy], :all)
    else
      can(:read, :all)
      can(:update, User) { |u| u.id == user.id }
      #cannot(:create, Course) # for now, only allow admins to upload courses
      can(:create, Course) if user.has_role?(:alpha_tester)
      can(:create, Essay) if user.has_role?(:creative)
      can([:update, :destroy], [Course, Essay], author: user)
      can([:update, :destroy], CourseChapter, course: { author: user })

      can :read, GamingEvent do |e|
        e.course_chapter.course.author == user || can?(:watch, e.course_chapter)
      end

      can([:create, :update, :destroy], GamingEvent) {|e| can?(:update, e.course_chapter)}
    end

    can :enrol, Course do |c|
      user != c.author && !c.students.include?(user) && c.course_chapters.present?
    end

    can :watch, Course do |c|
      user.accepted_course_ids.include?(c.id) && c.course_chapters.present?
    end

    can :watch, CourseChapter do |c|
      if can?(:watch, c.course)
        e = c.course.enrolments.where(user_id: user.id).first
        e.completed_at.present? || c.position <= e.current_chapter.position
      else
        false
      end
    end

    can :gamify, CourseChapter do |c|
      can?(:update, c) && !c.new_record? && !c.processing
    end

    can :discover, GamingEvent do |e|
      can? :watch, e.course_chapter
    end

    cannot(:destroy, User) { |u| u.id == user.id }
  end
end

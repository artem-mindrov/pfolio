require 'redis_extensions'

class User < ActiveRecord::Base
  include Redis::Objects
  include Redis::Extensions

  mount_uploader :avatar, AvatarUploader

  BIO_LIMIT   = 256
  NAME_LIMIT  = 48

  rolify
  extend FriendlyId
  friendly_id :name, use: [:slugged, :finders]

  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :authored_courses, class_name: "Course", foreign_key: :author_id
  has_many :enrolments, dependent: :destroy
  has_many :accepted_courses, through: :enrolments, source: :course
  has_many :score_activities, class_name: "ScoreActivity", foreign_key: :owner_id, dependent: :destroy
  has_many :essays

  sorted_set :scoreboard, global: true

  with_options if: ->(u) { u.has_role?(:creative) }, on: :create do |u|
    u.validates_url :website
    u.validates :website, presence: true
  end

  validates :email, format: { with: /\A(|(([A-Za-z0-9]+_+)|([A-Za-z0-9]+\-+)|([A-Za-z0-9]+\.+)|([A-Za-z0-9]+\++))*[A-Za-z0-9]+@((\w+\-+)|(\w+\.))*\w{1,63}\.[a-zA-Z]{2,6})\z/i }

  validates_integrity_of :avatar
  validates_processing_of :avatar

  with_options if: ->(u) { (u.approved? && !u.approved_changed? && u.confirmed?) && !u.has_role?(:admin) }, allow_blank: true do |u|
    u.validates :name, length: { in: 3..NAME_LIMIT }
    u.validates :bio, length: { in: 16..BIO_LIMIT }
  end

  before_create :approve_admin
  after_create :notify_admin, :add_user_to_mailchimp, :send_thanks
  after_create -> { confirm! if has_role?(:admin) }
  before_destroy :remove_user_from_mailchimp

  default_scope { order("score desc") }
  scope :lookup_by_email, ->(param) { where('users.email ilike ?', "%#{ param }%") if param }
  scope :lookup_by_email_or_name, ->(param) { where('users.email ilike ? or users.name ilike ?', "%#{ param }%", "%#{ param }%") if param }

  # prelaunch
  # override Devise method
  # no password is required when the account is created; validate password when the user sets one
  validates_confirmation_of :password
  def password_required?
    if !persisted?
      !(password != "")
    else
      !password.nil? || !password_confirmation.nil?
    end
  end

  def completed_courses
    accepted_courses.merge(enrolments.complete)
  end

  def incomplete_courses
    accepted_courses.merge(enrolments.incomplete)
  end

  # override Devise method
  def confirmation_required?
    false
  end

  # new function to determine whether a password has been set
  def has_no_password?
    self.encrypted_password.blank?
  end

  # new function to provide access to protected method pending_any_confirmation
  def only_if_unconfirmed
    pending_any_confirmation {yield}
  end

  def attempt_set_password(params)
    p = {}
    p[:password] = params[:password]
    p[:password_confirmation] = params[:password_confirmation]
    update_attributes(p)
  end
  # end prelaunch

  def active_for_authentication?
    super && approved? && (confirmed? || confirmation_period_valid?)
  end

  def inactive_message
    approved? ? super : :not_approved
  end

  def display_name
    self.name.blank? ? self.email : self.name
  end

  def to_s
    self.display_name
  end

  def self.send_reset_password_instructions(attributes={})
    recoverable = find_or_initialize_with_errors(reset_password_keys, attributes, :not_found)
    if !recoverable.approved?
      recoverable.errors[:base] << I18n.t("devise.failure.not_approved")
    elsif !recoverable.confirmed?
      recoverable.errors[:base] << I18n.t("devise.failure.unconfirmed")
    elsif recoverable.persisted?
      recoverable.send_reset_password_instructions
    end
    recoverable
  end

  def self.valid_attribute?(attr, value)
    mock = self.new(attr => value)
    if mock.valid?
      true
    else
      !mock.errors.has_key?(attr)
    end
  end

  def notify_admin
    AdminMailer.new_user_notify(self.id).deliver
  end

  private

  def approve_admin
    self.approved = true if has_role?(:admin)
  end

  def send_thanks
    UserMailer.thanks(self.id).deliver
  end

  def add_user_to_mailchimp
    return if ENV['ADMIN_EMAIL'].blank? || ENV['MAILCHIMP_LIST_ID'].blank? || self.email.include?(ENV['ADMIN_EMAIL'])
    mailchimp = Gibbon::API.new
    result = mailchimp.lists.subscribe({
      id: ENV['MAILCHIMP_LIST_ID'],
      email: { email: self.email },
      double_optin: false,
      update_existing: true,
      send_welcome: false
      })
    Rails.logger.info("Subscribed #{self.email} to MailChimp") if result
  rescue Gibbon::MailChimpError => e
    Rails.logger.info("MailChimp subscribe failed for #{self.email}: " + e.message)
  end

  def remove_user_from_mailchimp
    return if ENV['MAILCHIMP_LIST_ID'].blank?
    mailchimp = Gibbon::API.new
    result = mailchimp.lists.unsubscribe({
      id: ENV['MAILCHIMP_LIST_ID'],
      email: { email: self.email },
      delete_member: true,
      send_goodbye: false,
      send_notify: false
      })
    Rails.logger.info("Unsubscribed #{self.email} from MailChimp") if result
  rescue Gibbon::MailChimpError => e
    Rails.logger.info("MailChimp unsubscribe failed for #{self.email}: " + e.message)
  end
end

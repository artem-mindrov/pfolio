class ScoreActivity < ActiveRecord::Base
  belongs_to :owner, class_name: "User"
  belongs_to :subject, polymorphic: true

  validates :owner, :action, presence: true

  default_scope { order("date(created_at) desc") }
  scope :daily, -> { select(%{array_agg(subject_id) as subject_ids,
      sum(score) as score_total, action, subject_type, date(created_at) as date}).
      group("date(created_at), subject_type, action") }

  paginates_per 50

  def self.scores
    {
        Course: {
            enrol: 2 # given to student
        },
        Enrolment: {
            create: 1, # given to course author
            progress: ->(activity) { activity.subject.completed_at.present? ? 5 : 1 }
        },
        CourseChapter: {
            create: 2
        },
        GamingEvent: {
            create: 1,
            discover: 1
        }
    }
  end

  after_create :increment_owner_score
  after_destroy :decrement_owner_score

  before_save :set_score

  def text(count)
    I18n.t("activerecord.attributes.score_activity.actions.#{self.subject_type}.#{self.action}", count: count)
  end

  private

  def set_score
    if self.score.blank?
      score_value = self.class.scores[self.subject_type.to_sym][self.action.to_sym] rescue 0
      self.score = score_value.respond_to?(:call) ? score_value.call(self) : score_value
    end
  end

  def increment_owner_score
    if self.score
      User.scoreboard.incr(self.owner_id, self.score)
      User.update_counters(self.owner_id, score: self.score)
    end
  end

  def decrement_owner_score
    if self.score
      User.scoreboard.decr(self.owner_id, self.score)
      User.update_counters(self.owner_id, score: -self.score)
    end
  end
end

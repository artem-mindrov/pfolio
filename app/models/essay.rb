class Essay < ActiveRecord::Base
  WORD_LIMIT = 500
  acts_as_taggable_on :categories

  extend FriendlyId
  friendly_id :title, use: [:slugged, :finders]

  belongs_to :author, class_name: "User"

  validates :title, :author, presence: true
  validates :text, presence: true, length: { maximum: WORD_LIMIT,
                                             tokenizer: ->(str) { str.scan(/\w+/)},
                                             too_long: I18n.t("activerecord.errors.models.essay.text.too_long", count: WORD_LIMIT) }

  def to_s
    self.text.html_safe
  end
end

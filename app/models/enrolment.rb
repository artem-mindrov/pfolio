require 'score_activity_subject'

class Enrolment < ActiveRecord::Base
  include ScoreActivitySubject

  belongs_to :user
  belongs_to :course
  has_many :discoveries
  has_many :discovered_events, through: :discoveries, source: :gaming_event

  validates :user, :course, presence: true
  validates_uniqueness_of :course_id, scope: :user_id
  validate :check_progress

  before_save :check_completeness

  scope :complete, -> { where('enrolments.completed_at is not null') }
  scope :incomplete, -> { where('enrolments.completed_at is null') }

  def to_s
    self.course.title
  end

  def current_chapter
    self.course.course_chapters[self.progress % self.course.course_chapters.count]
  end

  private

  def check_completeness
    self.completed_at = (self.progress == self.course.course_chapters.count) ? Time.now : nil
  end

  def check_progress
    if self.progress > self.course.course_chapters.count
      self.errors.add(:progress, I18n.t("activerecord.errors.models.enrolment.attributes.progress.out_of_range"))
    end
  end
end

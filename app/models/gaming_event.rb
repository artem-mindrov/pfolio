require 'score_activity_subject'

class GamingEvent < ActiveRecord::Base
  include ScoreActivitySubject

  belongs_to :course_chapter
  has_many :discoveries

  validates :top, :left, :start, :duration, :text, :course_chapter_id, presence: true
  validates :top, :left, :start, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
  validates :top, :left, numericality: { less_than: 100 }
  validates :duration, numericality: { only_integer: true, greater_than_or_equal_to: 1 }
  validate :chapter_event_limit

  scope :discovered, ->(enrolment) { joins(:discoveries).where("discoveries.enrolment_id" => enrolment.id) }
  scope :undiscovered, ->(enrolment) {
    where("not exists (select * from discoveries d where d.enrolment_id = ? and d.gaming_event_id = gaming_events.id)",
          enrolment.id)
  }

  def to_s
    self.text.html_safe
  end

  private

  def chapter_event_limit
    if self.course_chapter.gaming_events.count >= CourseChapter::EVENT_LIMIT
      errors.add(:base, I18n.t("activerecord.errors.models.gaming_event.attributes.base.limit_reached"))
    end
  end
end

class SessionsController < Devise::SessionsController
  layout "landing_page", only: :new

  def destroy
    super
    reset_session
  end
end
class AuthoredCoursesController < ApplicationController
  layout "home"
  before_filter :authenticate_user!

  def index
    @courses = current_user.authored_courses.page(params[:page]).per(12)
    render template: "courses/index"
  end
end
# encoding: utf-8

class CoursesController < InheritedResources::Base
  include ActionView::RecordIdentifier

  before_filter :authenticate_user!, except: [:index, :show]
  before_filter :save_after_update_path, only: :edit
  after_filter :update_pageviews, only: :show

  load_and_authorize_resource
  skip_load_and_authorize_resource only: :watch
  respond_to :html, :js, :json

  def search
    js :index

    index! do |format|
      format.html { render :index }
    end
  end

  def new
    js course: @course.as_json(include: [:author, :course_chapters])
    new!
  end

  def edit
    js :new, course: @course.as_json(include: [:author, :course_chapters])
    edit!
  end

  def create
    js :new
    @course = Course.new(build_resource_params.first)
    set_upload_data

    respond_to do |format|
      if @course.save
        format.html { redirect_to course_course_chapters_path(@course) }
        format.json { render status: 200, json: @course }
      else
        flash[:error] = @course.errors.full_messages.join(", ")
        format.html { render :new }
        format.json { render status: 422, json: @course.errors }
      end
    end
  end

  def update
    js :new
    @course = Course.find(params[:id])
    set_upload_data

    respond_to do |format|
      if @course.update_attributes(build_resource_params.first)
        format.html { session.delete(:return_to) || @course }
        format.json { render status: 200, json: @course }
      else
        format.html { render :edit }
        format.json { render status: 422, json: @course.errors }
      end
    end
  end

  def build_resource_params
    [params.fetch(:course, {}).permit(:author_id, :title, :intro, :description, :fee)]
  end

  def collection
    @search = end_of_association_chain.viewable.search(params[:q])
    @courses = @search.result.page(params[:page]).per(12)
  end

  def reorder
    respond_to do |format|
      status, msg = resource.reorder(params[:order])

      if status
        format.js { render status: 200, nothing: true }
      else
        flash[:error] = t("controllers.courses.reorder.error")
        format.js { render status: 500, text: msg }
      end
    end
  end

  def enrol
    @course = Course.find(params[:id])

    respond_to do |format|
      if current_user.accepted_courses << @course
        add_score(@course)
        add_score(@course.enrolments.where(user_id: current_user.id).last, owner: @course.author, action: "create")
        flash[:notice] = t("controllers.courses.enrol.success")
        format.html { redirect_to watch_course_path(@course) }
        format.js
      else
        flash[:error] = t("controllers.courses.enrol.error")
        format.html { redirect_to :back }
        format.js { render status: 500, text: flash[:error] }
      end
    end
  end

  def watch
    @course = Course.find(params[:id])
    @enrolment = @course.enrolments.where(user_id: current_user.id).first
    @current_chapter = params[:c] ? CourseChapter.find(params[:c]) : @enrolment.current_chapter
    authorize! :watch, @current_chapter
    js enrolment: @enrolment.slice("id", "completed_at"),
       discovered: @current_chapter.gaming_events.discovered(@enrolment),
       undiscovered: @current_chapter.gaming_events.undiscovered(@enrolment),
       target: dom_id(@current_chapter)
  end

  private

  def set_upload_data
    if key = params[:course_key]
      @course.intro.key = key
      @course.s3_multipart_upload = S3Multipart::Upload.find_by(key: key)
    end
  end

  def update_pageviews
    if user_signed_in?
      resource.user_views << current_user.id
    else
      resource.anonymous_views << IPAddr.new(request.remote_ip).hton
    end
  end
end
# encoding: utf-8

class CourseChaptersController < InheritedResources::Base
  belongs_to :course
  before_filter :authenticate_user!
  before_filter :save_after_update_path, only: [:new, :edit]

  load_and_authorize_resource
  respond_to :html, :js, :json

  def index
    @course_chapter = @course.course_chapters.build
    index!
    js course_id: @course.id, chapters: collection
  end

  def new
    new! do |success|
      success.html { render layout: !request.xhr? }
    end
  end

  def create
    @course_chapter = CourseChapter.new(build_resource_params.first)
    set_upload_data

    respond_to do |format|
      if @course_chapter.save
        add_score(@course_chapter, owner: @course.course_chapter.author)
        format.html { redirect_to_next }
        format.json { render status: 200, json: @course_chapter }
      else
        format.html { render :new }
        format.json { render status: 422, json: @course_chapter.errors }
      end
    end
  end

  def update
    @course_chapter = CourseChapter.find(params[:id])
    set_upload_data

    respond_to do |format|
      if @course_chapter.update_attributes(build_resource_params.first)
        format.html { redirect_to_next }
        format.json { render status: 200, json: @course_chapter }
      else
        format.html { render :edit }
        format.json { render status: 422, json: @course_chapter.errors }
      end

    end
  end

  def gamify
    @course_chapter = CourseChapter.find(params[:id])
    js events: @course_chapter.gaming_events.to_a, limit: CourseChapter::EVENT_LIMIT
  end

  def build_resource_params
    [params.fetch(:course_chapter, {}).permit(:course_id, :title, :video, :description, :position)]
  end

  private

  def redirect_to_next
    if params[:commit] == I18n.t("course_chapters.new.done")
      redirect_to session.delete(:return_to) || resource.course
    else
      next_chapter = resource.course.course_chapters.where("id > ?", resource.id).order("id asc").first
      redirect_to (next_chapter && [resource.course, next_chapter]) || new_course_course_chapter_path(resource.course)
    end
  end

  def set_upload_data
    if key = params[:course_chapter_key]
      @course_chapter.video.key = key
      @course_chapter.s3_multipart_upload = S3Multipart::Upload.find_by(key: key)
    end
  end
end
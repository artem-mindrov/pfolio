class LandingPageController < ApplicationController
  # prelaunch
  #before_filter :redirect_signed_in, only: :index

  #private
  #
  #def redirect_signed_in
  #  redirect_to(home_path) if user_signed_in?
  #end

  def index
    @student, @creative = User.new, User.new(roles: [Role.where(name: "creative").first_or_create])
  end
end

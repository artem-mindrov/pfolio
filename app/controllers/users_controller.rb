class UsersController < InheritedResources::Base
  before_filter :authenticate_user!
  load_and_authorize_resource except: :avatar
  actions :index
  respond_to :html, :json, :js

  def index
    @users = User.lookup_by_email_or_name(params[:q]).limit(5) if params[:q]
    index!
  end

  def avatar
    resource = current_user

    respond_to do |format|
      if User.valid_attribute?(:avatar, params[:user][:avatar])
        resource.update_attribute(:avatar, params[:user][:avatar])
        flash[:notice] = t("controllers.users.avatar.success")
        format.js
      else
        flash[:error] = t("controllers.users.avatar.error")
        format.js { render status: 500, text: t("controllers.users.avatar.error") }
      end
    end
  end
end
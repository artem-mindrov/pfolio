class ApplicationController < ActionController::Base
  protect_from_forgery

  after_action :flash_to_headers, :store_location
  helper_method :for_device_type, :device_type

  rescue_from CanCan::AccessDenied do |exception|
    flash[:error] = exception.message

    respond_to do |format|
      format.html { redirect_to root_path}
      format.js { render status: 401, text: exception.message }
    end
  end

  def store_location
    if request.get? && !request.xhr?
      if request.fullpath !~ /\/confirmations|\/users/
        session[:previous_url] = request.fullpath
      end
    end
  end

  def save_after_update_path
    session[:return_to] ||= request.referer unless request.xhr?
  end

  def after_update_path_for(resource)
    session[:previous_url] || root_path
  end

  def after_sign_in_path_for(resource)
    after_update_path_for(resource)
  end

  def after_sign_out_path_for(resource)
    root_path
  end

  def after_sending_reset_password_instructions_path_for(resource_name)
    root_path
  end

  def flash_to_headers
    if request.xhr?
      #avoiding XSS injections via flash
      flash_json = Hash[flash.map{|k,v| [k,ERB::Util.h(v)] }].to_json
      response.headers['X-Flash-Messages'] = flash_json
      flash.discard
    end
  end

  # Executes a block of code only when a request was made by a given client device type.
  #
  # Example:
  #
  # for_device_type :mobile do
  # logger.info "This gets executed only for mobile phones."
  # end
  #
  # @param wanted_device_type a symbol identifying a device type
  # @yield a block to execute if the specified device type matches device type of the
  # current request
  # @return value that the yielded block returns, or nil if it didn't get executed
  def for_device_type(*wanted_device_types)
    raise ArgumentError, "Device helper takes a block of code to execute for given device." unless block_given?
    raise "Mobvious device type not set. Did you add the Mobvious rack middleware?" unless device_type

    (wanted_device_types.include? device_type) ? yield : nil
  end

  # @return [Symbol] device type for current request
  def device_type
    request.env['mobvious.device_type']
  end

  def add_score(subject, options = {})
    action = options.delete(:action) || params[:action]
    owner = options.delete(:owner) || current_user

    if options.key?(:limit) &&
        options[:limit] <= owner.score_activities.where(subject_id: subject.id,
                                                        subject_type: subject.class.name,
                                                        action: action).count
      return
    end

    owner.score_activities.create!(subject: subject, action: action)
  end
end

class EssaysController < InheritedResources::Base
  before_filter :authenticate_user!, except: [:index, :show]

  load_and_authorize_resource

  def build_resource_params
    [params.fetch(:essay, {}).permit(:author_id, :title, :text, :category_list)]
  end
end
class ScoreActivitiesController < ApplicationController
  layout "home"

  def index
    @user = User.find(params[:user_id])
    @score_activities = @user.score_activities.daily.page(params[:page])
  end
end
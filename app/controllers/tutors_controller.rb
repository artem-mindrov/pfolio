class TutorsController < InheritedResources::Base
  respond_to :html, :js

  def collection
    @tutors ||= User.with_role(:creative).includes(:authored_courses).page(params[:page]).per(10)
  end

  def show
    show! do |success|
      @courses = resource.authored_courses.page(params[:page]).per(10)
    end
  end

  def resource
    @tutor ||= User.with_role(:creative).find(params[:id])
  end
end
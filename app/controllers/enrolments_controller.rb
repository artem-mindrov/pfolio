class EnrolmentsController < ApplicationController
  layout "home"
  before_filter :authenticate_user!

  def index
    @completed = current_user.enrolments.complete.includes(:course).page(params[:c_page]).per(20)
    @incomplete = current_user.enrolments.incomplete.includes(:course).
        order('progress asc').page(params[:i_page]).per(12)
  end

  def progress
    @enrolment = Enrolment.find(params[:id])

    respond_to do |format|
      if @enrolment.update_attribute(:progress, @enrolment.progress + 1)
        add_score(@enrolment.reload)
        flash[:notice] = t("controllers.enrolments.progress.complete") if @enrolment.completed_at.present?
        format.js { render nothing: true, status: 200 }
      else
        flash[:error] = t("controllers.enrolments.progress.error")
        format.js { render status: 500, text: flash[:error] }
      end
    end

    js false # prevent paloma exec chains
  end
end
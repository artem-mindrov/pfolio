class Admin::ApplicationController < ApplicationController
  helper_method :sort_attr, :sort_direction, :invert_sort_direction
  before_action :authenticate_user!, :authorize

  private

  def authorize
    unless current_user.has_role?(:admin)
      redirect_to(session[:previous_url] || root_path, flash: { error: t("controllers.admin.application.authorize.unauthorized") })
    end
  end

  def sort_attr
    params[:sort_attr]
  end

  def sort_direction
    params[:sort_direction] || "asc"
  end

  def invert_sort_direction
    sort_direction == "asc" ? "desc" : "asc"
  end
end
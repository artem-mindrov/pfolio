class Admin::UsersController < Admin::ApplicationController
  inherit_resources
  respond_to :html, :js

  def index
    index! do |success|
      @users = @users.order("#{sort_attr} #{sort_direction}").
          lookup_by_email(params[:search]).
          page(params[:page]).per(params[:per_page])
      @users = @users.where(approved: params[:pending] != "on") if params[:pending]
    end
  end

  def show
    show! do |success|
      success.html { render partial: "show", layout: !request.xhr? }
    end
  end

  def approve
    @user = User.find(params[:id])
    @user.update_attribute(:approved, true)
    @user.send_confirmation_instructions

    respond_to do |format|
      format.js { render template: "admin/users/update" }
    end
  end

  def build_resource_params
    [params.require(:user).permit(:email, :password, :password_confirmation,
                                  :name, :bio, :website, :approved)]
  end

  private

  def sort_attr
    super || "email"
  end
end
class Admin::CoursesController < Admin::ApplicationController
  inherit_resources
  respond_to :html, :js

  def edit
    js "Courses#new", course: resource.as_json(include: :course_chapters)
    render template: "courses/edit"
  end

  def collection
    @search = end_of_association_chain.includes(:author)
      .order("#{sort_attr} #{sort_direction}").search(params[:q])
    @courses = @search.result.page(params[:page])
  end

  private

  def sort_attr
    if params[:sort_attr] == "author"
      "users.name || users.email"
    else
      super || "title"
    end
  end
end
class GamingEventsController < InheritedResources::Base
  before_filter :authenticate_user!
  load_and_authorize_resource
  respond_to :json
  actions :create, :update, :destroy

  def create
    create! do |success,failure|
      success.json do
        add_score(resource, owner: resource.course_chapter.course.author)
        respond_with(resource)
      end
    end
  end

  def discover
    @gaming_event = GamingEvent.find(params[:id])
    @gaming_event.discoveries.create(enrolment_id: params[:enrolment_id])
    add_score(@gaming_event)
    respond_with(@gaming_event)
  end

  def build_resource_params
    [params.require(:gaming_event).permit(:course_chapter_id, :top, :left, :start, :duration, :text)]
  end
end
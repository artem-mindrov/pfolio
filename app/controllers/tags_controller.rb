class TagsController < ApplicationController
  def index
    @tags = ActsAsTaggableOn::Tag.where("name ilike ?", "#{params[:q]}%").order(:name).limit(5)

    respond_to do |format|
      format.json { render json: @tags , only: [:id, :name] }
    end
  end
end
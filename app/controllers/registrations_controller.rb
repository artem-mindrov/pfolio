class RegistrationsController < Devise::RegistrationsController
  # prelaunch
  before_action :redirect_to_landing, except: :create
  before_action :configure_permitted_parameters

  # override #create to respond to AJAX with a partial
  def create
    build_resource(sign_up_params)
    @creative, @student = (params[:commit] == t("landing_page.index.teach")) ? [resource, User.new] : [User.new, resource]

    if resource.save
      if resource.active_for_authentication?
        sign_in(resource_name, resource)
        #(render(partial: 'thankyou', layout: false) && return) if request.xhr?
        respond_with resource do |format|
          format.html { redirect_to after_sign_up_path_for(resource) }
          format.js
        end
      else
        expire_data_after_sign_in!
        #(render(partial: 'thankyou', layout: false) && return) if request.xhr?
        respond_with resource do |format|
          format.html { redirect_to after_inactive_sign_up_path_for(resource) }
          format.js
        end
      end
    else
      clean_up_passwords resource
      # prelaunch
      #render :action => :new, :layout => !request.xhr?
      respond_to do |format|
        format.html { render "landing_page/index", layout: request.xhr? ? false : "landing_page" }
        format.js
      end
    end
  end
  # end prelaunch

  def update
    @user = current_user

    successfully_updated = if needs_password?(@user, params[:user])
      @user.update_with_password(account_update_params)
    else
      params[:user].delete(:current_password)
      @user.update_without_password(account_update_params)
    end

    if successfully_updated
      set_flash_message :notice, :updated
      sign_in @user, bypass: true
      redirect_to after_update_path_for(@user)
    else
      render "edit"
    end
  end

  def after_inactive_sign_up_path_for(resource)
    # the page prelaunch visitors will see after they request an invitation
    # unless Ajax is used to return a partial
    thankyou_path
  end

  def after_sign_up_path_for(resource)
    # the page new users will see after sign up (after launch, when no invitation is needed)
    redirect_to root_path
  end

  private

  def needs_password?(user, params)
    (params[:email].present? && user.email != params[:email]) || !params[:password].blank?
  end

  def redirect_to_landing
    redirect_to root_path unless request.fullpath != root_path
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) do |u|
      params.require(:user).permit(:email, :website, :name, role_ids: [])
    end

    devise_parameter_sanitizer.for(:account_update) do |u|
      params.require(:user).permit(:email, :password, :password_confirmation, :current_password,
                                   :name, :bio, :website, :approved, role_ids: [])
    end
  end
end
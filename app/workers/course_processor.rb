class CourseProcessor
  include SuckerPunch::Job

  def perform(attributes)
    attributes.symbolize_keys!
    klass = attributes[:model].constantize

    if attributes.key?(:id)
      klass.find(attributes[:id])
    else
      klass.new(attributes)
    end.save_and_process(now: true)
  end
end

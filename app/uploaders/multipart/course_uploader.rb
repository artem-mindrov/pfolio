class CourseUploader < ApplicationController
  extend S3Multipart::Uploader::Core

  attach :course, using: :intro, foreign_key: "s3_multipart_upload_id"
  #accept %w(wmv avi mp4 mkv mov mpeg)

  limit min: 5.megabytes, max: 80.megabytes

  on_complete do |upload, session|
    logger.info "Course intro upload completed to #{upload.location}, starting postprocessing..."
    upload.course.save_and_process
  end
end

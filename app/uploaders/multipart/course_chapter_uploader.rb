class CourseChapterUploader < ApplicationController
  extend S3Multipart::Uploader::Core

  attach :course_chapter, using: :video, foreign_key: "s3_multipart_upload_id"
  #accept %w(wmv avi mp4 mkv mov mpeg)

  limit min: 5.megabytes, max: 80.megabytes

  on_complete do |upload, session|
    logger.info "Course chapter upload completed to #{upload.location}, starting postprocessing..."
    upload.course_chapter.save_and_process
  end
end

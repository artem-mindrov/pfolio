# encoding: utf-8
require 'carrierwave/processing/mime_types'
require 'streamio-ffmpeg'

class VideoClipUploader < CarrierWave::Uploader::Base
  include CarrierWaveDirect::Uploader unless Rails.env.development?
  include CarrierWave::Video
  include CarrierWave::MimeTypes
  include CarrierWave::MiniMagick

  def store_dir
    if Rails.env.development?
      "uploads/#{model.class.to_s.underscore}/#{model.id}/#{mounted_as}"
    else
      "uploads/#{model.class.to_s.underscore}/#{mounted_as}"
    end
  end

  version :thumb_large do
    process :thumb

    def url(*args)
      model.try(:processing?) ? "placeholder-video.jpg" : super(args)
    end

    def full_filename(for_file)
      [for_file.chomp(File.extname(for_file)), "large", "png"].join(".")
    end
  end

  version :thumb_small, from_version: :thumb_large do
    # no way to do this from large thumb, https://github.com/carrierwaveuploader/carrierwave/issues/1164
    process resize_thumb: 480

    def url(*args)
      model.try(:processing?) ? "placeholder-video.jpg" : super(args)
    end

    def full_filename(for_file)
      [for_file.chomp(File.extname(for_file)), "small", "png"].join(".")
    end
  end

  [ :mp4, :ogv, :webm ].each do |v|
    version v, if: ->(version, opts) { version.file.content_type !~ /#{v}/ } do
      process encode_video: [v, resolution: :same]
      process change_content_type: v # set_content_type is defined by carrierwave but it doesn't set
                                     # the type to correct values for versions
                                     # https://github.com/rheaton/carrierwave-video/issues/17

      def full_filename(for_file)
        [for_file.chomp(File.extname(for_file)), version_name].join(".")
      end
    end
  end

  def change_content_type(content_type)
    self.file.content_type = "video/#{content_type}"
  end

  def thumb(width = nil)
    cache_stored_file! if !cached?

    movie = FFMPEG::Movie.new(current_path)
    thumb_path = "#{File.join(File.dirname(current_path), File.basename(path, File.extname(path)))}.png"

    movie.screenshot(thumb_path,
                     { resolution: width ? "#{width}x#{width / movie.calculated_aspect_ratio}" : movie.resolution,
                       seek_time: (movie.duration > 5) ? 5 : movie.duration / 2 },
                     preserve_aspect_ratio: :width)
    File.rename thumb_path, current_path
  end

  def resize_thumb(width)
    thumb_path = "#{File.join(File.dirname(current_path), File.basename(path, File.extname(path)))}.png"
    # have to change file extension here because the downloaded tmp file has the extension
    # of the original uploaded file, which confuses mogrify into writing the shrunken screenshot
    # as a video clip and breaking everything
    FileUtils.mv current_path, thumb_path
    image = ::MiniMagick::Image.open(thumb_path)
    height = (width / (image[:dimensions][0].to_f / image[:dimensions][1])).to_i
    image.resize "#{width}x#{height}"
    image.write(thumb_path)
    File.rename thumb_path, current_path
  end
end

describe "Picct.Views.Courses.Edit", ->
  beforeEach ->
    loadFixtures "courses/edit"

    chapters = []
    $("#sortable-chapters .chapter-item").each ->
      chapters.push
        title: $(@).find("input[name*=title]").val()
        id: $(@).find("[id^=course_chapter]").attr("id").match(/\d+$/)[0]
        course_id: $(@).find("[id$=course_id]").val()

    @course = new Picct.Models.Course
      id: chapters[0].course_id
      title: $("#course_title").val()
      course_chapters: chapters

    @course.course_chapters.course = @course
    @view = new Picct.Views.Courses.Edit(model: @course)
    @server = sinon.fakeServer.create()

  it "renders", ->
    expect(@view.$el).toEqual($("#direct-uploader"))
    expect($(".upload-control")).toBeHidden()

  it "has an uploader", ->
    expect(@view.model instanceof Picct.Models.Course).toBeTruthy()
    expect(@view.model.uploader instanceof DirectUploader).toBeTruthy()

  it "submits course data", ->
    @server.respondWith [200, "Content-Type": "application/json", JSON.stringify(@course)]
    spy = sinon.spy(@view, "chainedUpdateDone")

    @view.$(".save-all").trigger("click")
    expect(@view.$(".save-all")).toBeDisabled()

    @server.respond()
    expect(@view.$(".save-all")).not.toBeDisabled()
    expect(spy.calledOnce).toBeTruthy()

  it "submits all data using save button", ->
    spies = []
    @course.course_chapters.each (item) ->
      v = new Picct.Views.CourseChapters.Edit(model: item)
      v.enableUpdate()
      spies.push( sinon.spy(item, "save") )

    @server.respondWith [200, "Content-Type": "application/json", JSON.stringify(@course)]
    done = sinon.spy(@view, "chainedUpdateDone")

    @view.$(".save-all").trigger("click")
    expect(@view.$(".save-all")).toBeDisabled()

    @server.respond()

    _.each spies, (spy) =>
      @server.respond()
      expect(spy.calledOnce).toBeTruthy()

    expect(@view.$(".save-all")).not.toBeDisabled()
    expect(done.calledOnce).toBeTruthy()
    expect($(".chapter-item")).not.toHaveClass("panel-error")
    expect($(".chapter-item")).toHaveClass("panel-success")

  afterEach ->
    @server.restore()
describe "Picct.Views.CourseChapters.Sortable", ->
  dropToTail = ->
    item = $(".sortable .chapter-item:first-child")
    item.remove().appendTo(".sortable")
    @view.$(".sortable").triggerHandler("sortupdate", item: item)

  dropToHead = ->
    item = $(".sortable .chapter-item:last-child")
    item.remove().prependTo(".sortable")
    @view.$(".sortable").triggerHandler("sortupdate", item: item)

  beforeEach ->
    loadFixtures "courses/edit"

    chapters = []
    $("#sortable-chapters .chapter-item").each ->
      chapters.push
        title: $(@).find("input[name*=title]").val()
        id: $(@).find("[id^=course_chapter]").attr("id").match(/\d+$/)[0]
        course_id: $(@).find("[id$=course_id]").val()

    @course = new Picct.Models.Course
      id: chapters[0].course_id
      title: $("#course_title").val()
      course_chapters: chapters

    @course.course_chapters.course = @course
    @chapters = @course.course_chapters
    @view = new Picct.Views.CourseChapters.Sortable(container: "#sortable-chapters", collection: @chapters)

  it "renders", ->
    expect(@view.$el).toEqual($("#sortable-chapters"))
    expect(parseInt(@view.$("em").css("opacity"))).toEqual(1)

  it "adds chapters", ->
    spy = sinon.spy(Picct.Views.CourseChapters.Edit.prototype, "initialize")
    @chapters.add(new Picct.Models.CourseChapter(course: @course))
    expect(spy.calledOnce).toBeTruthy()

  it "reloads chapters after adding", ->
    spy = sinon.spy(jQuery.prototype, "sortable")
    @chapters.add(new Picct.Models.CourseChapter(course: @course))
    expect(spy.calledWith("reload")).toBeTruthy()

  it "detects sortable updates", ->
    @chapters.first().dirty = true
    dropToTail.apply(@)
    expect(@chapters.reordered).toBeTruthy()

  it "resets position values when proximity limit is reached", ->
    $(".sortable .chapter-item:first-child input[name*=position]").val(1)
    spy = sinon.spy(@view, "resetPositions")
    dropToHead.apply(@)
    expect(spy.calledOnce).toBeTruthy()

  it "updates order", ->
    dropToTail.apply(@)
    server = sinon.fakeServer.create()
    spy = sinon.spy(jQuery.prototype, "fadeOut")
    server.respondWith [200, "Content-Type": "application/json", JSON.stringify(@course)]
    @view.$(".save-order").trigger("click")
    server.respond()
    expect(spy.calledOnce).toBeTruthy()
    server.restore()
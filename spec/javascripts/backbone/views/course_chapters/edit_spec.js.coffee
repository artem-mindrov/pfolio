describe "Picct.Views.CourseChapters.Edit", ->
  beforeEach ->
    loadFixtures "courses/edit"
    @course = new Picct.Models.Course
      id: _.last($("#direct-uploader .main-form form").attr("action").split("/"))
      title: $("#course_title").val()

  it "appends a new form for a new instance", ->
    view = new Picct.Views.CourseChapters.Edit(model: new Picct.Models.CourseChapter(course: @course))
    expect(view.$(".main-form form").attr("action")).toEqual("/courses/#{@course.id}/course_chapters")

  it "uses an existing form for an existing chapter", ->
    id = $("#sortable-chapters .chapter-item:first-child [id^=course_chapter]").attr("id").match(/\d+$/)[0]
    view = new Picct.Views.CourseChapters.Edit(model: new Picct.Models.CourseChapter(
      course: @course
      id: id))
    expect(view.$(".main-form form").attr("action")).toEqual("/courses/#{@course.id}/course_chapters/#{id}")

  it "binds event handlers", ->
    view = new Picct.Views.CourseChapters.Edit(model: new Picct.Models.CourseChapter(course: @course))

    view.$(".panel-heading").trigger("click")
    expect(view.$(".panel-collapse")).toBeVisible()

  it "runs chained updates", ->
    view = new Picct.Views.CourseChapters.Edit(model: new Picct.Models.CourseChapter(course: @course))
    server = sinon.fakeServer.create()

    view.enableUpdate()
    expect(view.$("a.update")).toBeVisible()

    server.respondWith [200, "Content-Type": "application/json", JSON.stringify(view.model)]
    spy = sinon.spy(view, "updateNext")
    view.$("a.update").trigger("click")
    server.respond()
    expect(spy.calledOnce).toBeTruthy()

    server.respondWith [500, "Content-Type": "application/json", JSON.stringify(error: "unknown error")]
    view.$("a.update").trigger("click")
    server.respond()
    expect(spy.calledOnce).toBeTruthy()

    server.restore()
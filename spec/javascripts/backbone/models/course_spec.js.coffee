describe "Picct.Models.Course", ->
  it "instantiates with chapters", ->
    c = new Picct.Models.Course
      title: "Course"
      course_chapters: [
        { title: "chapter 1" }
        { title: "chapter 2" }
        { title: "chapter 3" }
      ]

    expect(c.course_chapters instanceof Picct.Models.CourseChapters).toBeTruthy()
    expect(c.course_chapters.length).toEqual(3)

    c.course_chapters.each (item) ->
      expect(item.get("title")).toEqual("chapter #{c.course_chapters.indexOf(item) + 1}")
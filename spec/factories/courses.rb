# Read about factories at https://github.com/thoughtbot/factory_girl
include CarrierWaveDirect::Test::Helpers

FactoryGirl.define do
  factory :course do
    author
    sequence(:title) {|n| "Course #{n}i"}
    key { sample_key(VideoClipUploader.new(Course.new, "intro")) }

    factory :course_with_chapters do
      after(:create) do |course|
        create_list(:course_chapter, 2, course: course)
      end
    end

    factory :course_with_incomplete_chapters do
      after(:create) do |course|
        create_list(:course_chapter, 2, course: course, processing: true)
      end
    end
  end
end

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :gaming_event do
    course_chapter
    top 0
    left 0
    start 0
    duration 1
    text "some text"
  end
end

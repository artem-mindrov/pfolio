# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :essay do
    author
    title "essay"
    text "text"
  end
end

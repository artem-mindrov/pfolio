# Read about factories at https://github.com/thoughtbot/factory_girl
include CarrierWaveDirect::Test::Helpers

FactoryGirl.define do
  factory :course_chapter do
    course
    sequence(:title) {|n| "Chapter #{n}i"}
    key { sample_key(VideoClipUploader.new(CourseChapter.new, "video")) }
    sequence(:position) {|n| n*16}
  end
end

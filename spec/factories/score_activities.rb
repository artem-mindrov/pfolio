# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :score_activity do
    owner
    subject
    action "create"
  end
end

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user, aliases: [:author, :owner] do
    sequence(:email) { |n| "user#{n}@test.com" }
    password 'password'
    password_confirmation 'password'
    website "http://some.site.com"

    %w{ admin creative student}.each do |role|
      factory role do
        after(:build) { |user| user.add_role(role) }
      end
    end
  end
end

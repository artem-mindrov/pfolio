# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :discovery do
    enrolment
    gaming_event
  end
end

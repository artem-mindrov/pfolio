require 'spec_helper'
require 'active_model'

class Validatable
  include ActiveModel::Validations
  attr_accessor :url
  validates_url :url
end

describe UrlValidator do
  subject { Validatable.new }

  context 'invalid input' do
    it 'should return false for a poorly formed URL' do
      subject.stub(:url).and_return('something.com')
      expect( subject ).to_not be_valid
    end

    it 'should return false for garbage input' do
      subject.stub(:url).and_return(3.14159265)
      expect( subject ).to_not be_valid
    end

    it 'should return false for URLs without an HTTP protocol' do
      subject.stub(:url).and_return('ftp://secret-file-stash.net')
      expect( subject ).to_not be_valid
    end
  end

  context 'valid input' do
    it 'should return true for a correctly formed HTTP URL' do
      subject.stub(:url).and_return('http://nooooooooooooooo.com')
      expect( subject ).to be_valid
    end

    it 'should return true for a correctly formed HTTPS URL' do
      subject.stub(:url).and_return('https://google.com')
      expect( subject ).to be_valid
    end
  end
end
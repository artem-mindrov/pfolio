require 'spec_helper'

feature "course management", js: true do
  before(:each) do
    register_webmock_stubs
    @user = FactoryGirl.create(:admin)
    login_as(@user, scope: :user)
  end

  context "uploads", driver: :selenium_remote_firefox_billy do
    scenario "creating a course" do
      ids = add_course_with_chapters
      click_on I18n.t("save")
      expect(page).to have_content(I18n.t("uploading", filename: fixture_filename), wait: 0.3)
      expect_all_uploads_to_complete(ids)
    end

    scenario "creating chapters" do
      course = FactoryGirl.create(:course)
      visit edit_course_path(course)

      ts = add_chapter
      enable_file_inputs

      fill_in_chapter_fields(ts)
      page.find("a.update").click

      expect(page).to have_content(I18n.t("uploading", filename: fixture_filename), wait: 0.3)
      expect_all_uploads_to_complete([ts])
    end
  end

  scenario "pause course upload" do
    ids = add_course_with_chapters
    click_on I18n.t("save")

    click_upload_control_button("pause")
    click_upload_control_button("resume")

    # cant test for completed uploads since for some reason freaking firefox webdriver
    # will abort subsequent request so uploads will never complete
    ([ "#course-upload" ] | ids.map {|id| "##{id}-new-chapter" }).each do |container|
      expect(page).to_not have_css("#{container} .resume")
      expect(page).to have_css("#{container} .pause")
      expect(page).to have_css("#{container} .cancel")
    end
  end

  scenario "cancel course upload" do
    ids = add_course_with_chapters
    click_on I18n.t("save")

    click_upload_control_button("cancel")

    expect(page).to_not have_css("#upload-status")
    expect(page).to have_css("#save-all")
    %w{ pause resume cancel }.each { |btn| expect(page).to_not have_css(".#{btn}") }
  end

  context "validations" do
    scenario "course form" do
      visit new_course_path
      click_on I18n.t("save")

      expect(page).to have_js_validation_error_on("*[@id='save-all']")
      remove_error_popups

      enable_file_inputs

      within "#course-upload" do
        attach_remote_file(fixture_path)
      end

      click_on I18n.t("save")

      expect(page).to have_js_validation_error_on("input[@id='course_title']")
      remove_error_popups
    end

    scenario "course chapter form" do
      course = FactoryGirl.create(:course)
      visit edit_course_path(course)

      find("#sortable-chapters > div.text-center").click
      chapter_bar = find("#chapter-container .chapter-item:first-child")
      chapter_bar.find("[data-toggle=collapse]").click

      ts = chapter_bar[:id].split("-").first
      input = find_field("#{ts}-_course_chapter_title")
      input.click
      tab_away_from(input)

      expect(page).to have_js_validation_error_on("input[@id='#{ts}-_course_chapter_title']")
      remove_error_popups

      fill_in input[:id], with: "Title"
      input.native.send_keys(:tab)

      within "#chapter-container .chapter-item:first-child" do
        find("a.update").click
        expect(page).to have_js_validation_error_on("*[@id='#{ts}-new_course_chapter']//*[contains(@class,'dropzone')]")
      end
    end
  end

  # no fuckin way to make this work w/ selenium...
  #scenario "reordering" do
  #  course = FactoryGirl.create(:course_with_chapters)
  #  chapters = course.course_chapters.to_a
  #
  #  visit edit_course_path(course)
  #  source = find("#chapter-container .chapter-item:first-child .sortable-handle")
  #  target = find("#chapter-container")
  #  height = page.execute_script("return $('#chapter-container')[0].offsetHeight;")
  #  drag(source, target, 0, height - 25)
  #  click_on I18n.t("save_order")
  #
  #  expect(page).to_not have_css("#save-all")
  #  expect(course.reload.course_chapters).to eq(chapters.reverse)
  #end

  def add_course_with_chapters
    visit new_course_path
    fill_in "course_title", with: "Course Title"

    ids = [ add_chapter, add_chapter ]

    expect(page).to have_content(I18n.t("courses.form.hint.edit_chapters").split("<br/>").join(" "))

    enable_file_inputs

    within "#course-upload" do
      attach_remote_file(fixture_path)
    end

    expect(page).to have_css("#course-upload .progress")
    ids.each { |id| fill_in_chapter_fields(id) }
    ids
  end

  def add_chapter
    find("#sortable-chapters > div.text-center").click
    chapter_bar = find("#chapter-container .chapter-item:last-child")
    chapter_bar.find("[data-toggle=collapse]").click

    chapter_bar[:id].split("-").first
  end

  def fill_in_chapter_fields(id)
    within "##{id}-new_course_chapter" do
      attach_remote_file(fixture_path)
      fill_in "#{id}-_course_chapter_title", with: "Chapter #{id}"
    end
  end

  def expect_all_uploads_to_complete(ids = [])
    sleep ((ids.size + 1) * 5).seconds # should be enough for fake uploads to finish ?
    expect(page).to have_content(I18n.t("upload_complete"))
    expect(page).to_not have_css(".panel-danger")

    chapters = page.all(".panel-success")
    expect(chapters.size).to eq(ids.size)
  end

  def fixture_filename
    "test.mp4"
  end

  def fixture_path
    "spec/fixtures/video/#{fixture_filename}"
  end

  def enable_file_inputs
    page.execute_script(%Q{$("input[type=file]").css({'display': 'block'});})
  end

  def click_upload_control_button(action)
    expect(page).to have_css("#course-upload a.#{action}", visible: true)
    find("#course-upload a.#{action}").click
  end

end
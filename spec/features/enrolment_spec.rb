require 'spec_helper'

feature "enrolments", js: true do
  before do
    @user = FactoryGirl.create(:student, approved: true, confirmed_at: Time.now)
    @course = FactoryGirl.create(:course_with_chapters)
    login_as(@user)
  end

  scenario "enrolling in a course" do
    visit root_path
    click_on "profile"
    click_on I18n.t("navigation.courses")

    expect(current_path).to eq(courses_path)
    expect(page).to have_content(@course.title)

    within "#course-list" do
      find("li .btn-success").click
    end

    expect(page).to have_content(I18n.t("controllers.courses.enrol.success"))

    visit root_path

    expect(page).to_not have_content(I18n.t("enrolments.complete.header"))
    expect(page).to have_content(I18n.t("enrolments.incomplete.header"))
    expect(page).to have_content(@course.title)
  end

  context "watching a course" do
    before do
      enrolment = FactoryGirl.create(:enrolment, course: @course, user: @user)
      visit root_path

      find("a[href='#{watch_course_path(@course)}']").click
      expect(current_path).to eq(watch_course_path(@course))
    end

    scenario "navigating using chapter list" do
      @course.course_chapters.each do |chapter|
        using_wait_time(10) do # this takes longer for some reason
          find("#chapter-list a[href='#{watch_course_path(id: @course.id, c: chapter.id)}']").click
        end

        expect_user_to_watch(chapter)

        page.execute_script("$('video').trigger('ended')")
      end

      expect_user_to_complete(@course)
    end

    scenario "navigating using prev/next links" do
      @course.course_chapters.each do |chapter|
        page.execute_script("$('video').trigger('ended')")

        if chapter.next
          using_wait_time(10) do # this takes longer for some reason
            find("#current-chapter a[href='#{watch_course_path(id: @course.id, c: chapter.next.id)}']").click
          end

          expect_user_to_watch(chapter.next)
        end
      end

      expect_user_to_complete(@course)
    end
  end

  def expect_user_to_watch(chapter)
    using_wait_time(10) do
      expect(page).to_not have_css(".loading-overlay", visible: true)
      expect(page).to have_css("h3", text: chapter.title)
    end

    src = page.execute_script("return $('video source').attr('src');")
    expect(src).to match(/#{File.dirname(chapter.key)}/)
  end

  def expect_user_to_complete(course)
    expect(page).to have_content(I18n.t("controllers.enrolments.progress.complete"))

    visit root_path

    expect(page).to have_content(I18n.t("enrolments.complete.header"))
    expect(page).to_not have_content(I18n.t("enrolments.incomplete.header"))
    expect(page).to have_content(course.title)
  end
end
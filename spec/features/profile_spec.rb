require 'spec_helper'

feature "profile editing", js: true do
  before do
    @user = FactoryGirl.create(:creative, approved: true, confirmed_at: Time.now)
    login_as(@user, scope: :user)
    visit root_path
    click_on "profile"
    click_on I18n.t('navigation.my_account')
  end

  scenario "avatar upload" do
    page.find("div[data-target='#upload-avatar']").click

    page.execute_script(%Q{$("#avatar-uploader div.user_avatar").css({'display': 'block'});})

    within "#avatar-uploader" do
      attach_remote_file("spec/fixtures/pictures/tto.jpg")
      click_on I18n.t("save")
    end

    expect(page).to have_content(I18n.t("controllers.users.avatar.success"))
    expect(page.find("#avatar-uploader button[type=submit]")["disabled"]).to_not be_nil
  end

  scenario "general info" do
    within "#general-info" do
      fill_in I18n.t("activerecord.attributes.user.name"), with: "Name"
      fill_in I18n.t("activerecord.attributes.user.bio"), with: "A short biography"
    end

    expect_successful_update
  end

  scenario "change password" do
    page.find("div[data-target='#change-password']").click

    within "#change-password" do
      fill_in I18n.t("activerecord.attributes.user.password"), with: "password"
      fill_in I18n.t("activerecord.attributes.user.password_confirmation"), with: "password"
      fill_in I18n.t("activerecord.attributes.user.current_password"), with: @user.password
    end

    expect_successful_update
  end

  context "validations" do
    scenario "passwords" do
      page.find("div[data-target='#change-password']").click

      within "#change-password" do
        fill_in I18n.t("activerecord.attributes.user.password"), with: "password"
      end

      click_on I18n.t("update")
      expect(page).to have_xpath("//input[@id='user_password_confirmation']/preceding-sibling::div[contains(@class, 'formError')][1]")
      expect(page).to have_xpath("//input[@id='user_current_password']/preceding-sibling::div[contains(@class, 'formError')][1]")

      remove_error_popups

      within "#change-password" do
        fill_in I18n.t("activerecord.attributes.user.password"), with: "password"
        fill_in I18n.t("activerecord.attributes.user.current_password"), with: @user.password
      end

      click_on I18n.t("update")
      expect(page).to have_xpath("//input[@id='user_password_confirmation']/preceding-sibling::div[contains(@class, 'formError')][1]")
    end
  end

  scenario "character limit counters" do
    within "#general-info" do
      fill_in I18n.t("activerecord.attributes.user.name"), with: "Name"
      expect(page).to have_content(I18n.t("bootstrap.limit.counter_text") + (User::NAME_LIMIT - 4).to_s)

      fill_in I18n.t("activerecord.attributes.user.bio"), with: "Bio"
      expect(page).to have_content(I18n.t("bootstrap.limit.counter_text") + (User::BIO_LIMIT - 3).to_s)
    end
  end

  def expect_successful_update
    click_on I18n.t("update")

    expect(current_path).to eq(root_path)
    expect(page).to have_content(I18n.t("devise.registrations.updated"))
  end
end
require 'spec_helper'

feature "viewing scores", js: true do
  before do
    @user = FactoryGirl.create(:creative, approved: true, confirmed_at: Time.now)

    2.times do
      FactoryGirl.create(:course_with_chapters, author: @user)
      Course.last.course_chapters.each {|c| @user.score_activities.create(subject: c, action: "create") }

      Timecop.travel(2.days.from_now)
    end

    2.times do
      @user.score_activities.create(subject: FactoryGirl.create(:gaming_event, course_chapter: CourseChapter.last),
                                    action: "create")
    end

    login_as @user, scope: :user
  end

  scenario "view course authorship scores by date" do
    visit user_score_activities_path(user_id: @user.id)

    expect(page).to have_text(I18n.t("score_activities.index.total", count: @user.reload.score))
    expect(page).to have_text(I18n.t("activerecord.attributes.score_activity.actions.CourseChapter.create", count: Course.last.course_chapters.count))
    expect(page).to have_text(I18n.t("activerecord.attributes.score_activity.actions.GamingEvent.create", count: GamingEvent.count))
  end
end
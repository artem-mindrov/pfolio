require 'spec_helper'

feature "sign up and login", js: true do
  scenario "sign up as a tutor" do
    visit root_path
    fill_in I18n.t("landing_page.index.email"), with: "email@server.org"
    fill_in I18n.t("landing_page.index.website"), with: "picct.me"
    click_on I18n.t("landing_page.index.teach")

    expect(page).to have_css("#thankyou-creative", visible: true)

    within "#thankyou-creative" do
      expect(page).to have_content(I18n.t("registrations.thankyou.thanks"))
    end
  end

  scenario "sign up as a student" do
    visit root_path
    click_on I18n.t("landing_page.index.students")

    within "#students-sign-up" do
      fill_in I18n.t("landing_page.index.email"), with: "email@server.org"
      click_on I18n.t("landing_page.index.enrol")
    end

    expect(page).to have_css("#thankyou-student", visible: true)

    within "#thankyou-student" do
      expect(page).to have_content(I18n.t("registrations.thankyou.thanks"))
    end
  end

  scenario "sign in as unconfirmed" do
    user = sign_in(:creative, approved: true)
    expect(current_path).to eq(root_path)
  end

  scenario "sign in as a confirmed tutor" do
    user = confirm_and_sign_in(:creative)
    expect(current_path).to eq(tutor_root_path)
  end

  scenario "sign in as a confirmed student" do
    user = confirm_and_sign_in(:creative)
    expect(current_path).to eq(student_root_path)
  end

  scenario "password recovery" do
    u = FactoryGirl.create(:student, approved: true, confirmed_at: Time.now)

    visit root_path
    click_on I18n.t("landing_page.index.sign_in")

    within "#sign-in-modal" do
      click_on I18n.t("devise.sessions.new.forgot_password")
    end

    expect(current_path).to eq(new_user_password_path)

    fill_in I18n.t("activerecord.attributes.user.email"), with: u.email
    click_on I18n.t("devise.passwords.new.resend")

    visit_mail_link(check_mail(to: u, body: edit_user_password_path), :first)
    expect(page).to have_content(I18n.t("registrations.change_password.header"))

    newpwd = "newpassword"
    fill_in I18n.t("activerecord.attributes.user.new_password"), with: newpwd
    fill_in I18n.t("activerecord.attributes.user.confirm_new_password"), with: newpwd
    click_on I18n.t("devise.passwords.edit.change")

    expect(current_path).to eq(student_root_path)
    expect(page).to have_content(I18n.t("devise.passwords.updated"))
  end

  context "activation instruction recovery" do
    scenario "with an existing password" do
      u = FactoryGirl.create(:student, approved: true)
      trigger_activation(u)
    end

    scenario "without a password" do
      u = FactoryGirl.create(:student, approved: true, password: nil, password_confirmation: nil)

      trigger_activation(u) do
        expect(page).to have_content(I18n.t("devise.confirmations.show.activation"))

        u.password = u.password_confirmation = "password"
        fill_in I18n.t("devise.confirmations.show.choose"), with: u.password
        fill_in I18n.t("devise.confirmations.show.confirm"), with: u.password
        click_on I18n.t("devise.confirmations.show.activate")
      end
    end
  end

  def sign_in(user, options = {})
    if user.is_a?(Symbol)
      options = { password: "pa$$word", password_confirmation: "pa$$word" }.merge(options)
      user = FactoryGirl.create(user, options)
    end

    visit root_path if current_path != root_path
    click_on I18n.t("landing_page.index.sign_in")

    within "#sign-in-modal" do
      fill_in I18n.t("activerecord.attributes.user.email"), with: user.email
      fill_in I18n.t("activerecord.attributes.user.password"), with: user.password
      click_on I18n.t("devise.sessions.new.sign_in")
    end

    @current_user = user
  end

  def confirm_and_sign_in(role)
    user = FactoryGirl.create(role, approved: true, confirmed_at: Time.now)
    sign_in(user)
  end

  def check_mail(options = {})
    email = ActionMailer::Base.deliveries.last
    options[:to] = options[:to].email if options[:to].respond_to?(:email)
    expect(email.to).to include(options[:to]) if options[:to]
    expect(email.subject).to match(Regexp.new(options[:subject])) if options[:subject]

    if options[:body]
      if email.multipart?
        email.parts.each do |part|
          expect(part.body).to match(Regexp.new(options[:body]))
        end
      else
        expect(email.body).to match(Regexp.new(options[:body]))
      end
    end

    email
  end

  def visit_mail_link(mail, index)
    index = { nil => 0, first: 0, second: 1, third: 2, last: -1 }[index] if index.is_a?(Symbol)
    body = mail.multipart? ? (mail.text_part ? mail.text_part.body.decoded : nil) : mail.body.decoded
    visit URI::parse(body.scan(/https?:\/\/[^\s"><%#]+/)[index]).request_uri
  end

  def trigger_activation(user)
    visit root_path
    click_on I18n.t("landing_page.index.sign_in")

    within "#sign-in-modal" do
      click_on I18n.t("devise.sessions.new.resend_confirmation")
    end

    expect(current_path).to eq(new_user_confirmation_path)

    fill_in I18n.t("activerecord.attributes.user.email"), with: user.email
    click_on I18n.t("devise.confirmations.new.resend")

    visit_mail_link(check_mail(to: user, body: user_confirmation_path), :last)

    yield if block_given?

    expect(current_path).to eq(root_path)
    expect(page).to have_content(I18n.t("devise.confirmations.confirmed"))
  end
end
require 'spec_helper'

feature "browsing tutor profiles", js: true do
  before do
    @authors = FactoryGirl.create_list(:creative, 25, approved: true, confirmed_at: Time.now)
    @student = FactoryGirl.create(:student, approved: true, confirmed_at: Time.now)

    FactoryGirl.create_list(:course_with_chapters, 25, author: User.with_role(:creative).first)
    login_as(@student)
    visit root_path
  end

  scenario "browsing tutors" do
    click_on "profile"
    click_on I18n.t("navigation.tutors")

    expect(current_path).to eq(tutors_path)
    expect_pagination_for("#tutor-list", tutors, 12)
  end

  scenario "viewing tutor profiles" do
    click_on "profile"
    click_on I18n.t("navigation.tutors")
    expect(current_path).to eq(tutors_path)
    tutor = tutors.page(1).per(12).first
    first(".media-body").click_link(tutor)

    expect(current_path).to eq(tutor_path(tutor))
    expect(page).to have_content(tutor)
    expect(page).to have_content(tutor.bio)

    expect_pagination_for("#course-list", courses(tutor), 12)
  end

  def tutors
    User.with_role(:creative)
  end

  def courses(author)
    author.authored_courses
  end
end
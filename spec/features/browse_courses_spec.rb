require 'spec_helper'

feature "course browsing", js: true do
  before do
    @author = FactoryGirl.create(:creative, approved: true, confirmed_at: Time.now)
    @student = FactoryGirl.create(:student, approved: true, confirmed_at: Time.now)
    FactoryGirl.create_list(:course_with_chapters, 50, author: @author).each_with_index do |course,i|
      FactoryGirl.create(:enrolment, course: course, user: @student,
                         progress: i % course.course_chapters.count + 1)
    end
  end

  context "student enrolments" do
    before { browse_enrolments }

    scenario "finished" do
      expect_pagination_for("#completed-courses", complete_enrolments, 20)
      expect(page).to have_content(incomplete_enrolments.first.course.title)
      expect(page).to_not have_content(incomplete_enrolments.page(2).per(12).first.course.title)

      course = complete_enrolments.page(2).per(20).first.course
      click_on(course.title)
      expect(current_path).to eq(course_path(course))
    end

    scenario "watch again" do
      course = complete_enrolments.first.course
      expect(page).to have_css("a[href='#{watch_course_path(course)}']")

      first("a[href='#{watch_course_path(course)}']").click
      expect(current_path).to eq(watch_course_path(course))
    end

    scenario "unfinished" do
      expect_pagination_for("#incomplete-courses", incomplete_enrolments, 12)
      expect(page).to have_content(complete_enrolments.first.course.title)
      expect(page).to_not have_content(complete_enrolments.page(2).per(20).first.course.title)
    end
  end

  scenario "browse authored courses" do
    browse_authored_courses
    expect_pagination_for("#course-list", @author.authored_courses, 12)
  end

  def browse_enrolments
    login_as(@student)
    visit root_path
  end

  def browse_authored_courses
    login_as(@author)
    visit root_path
  end

  def complete_enrolments
    @student.enrolments.complete.includes(:course)
  end

  def incomplete_enrolments
    @student.enrolments.incomplete.includes(:course).order("progress asc")
  end
end
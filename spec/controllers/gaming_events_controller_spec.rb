require 'spec_helper'

describe GamingEventsController do
  context "scores" do
    it "gives points for creating" do
      course = FactoryGirl.create(:course_with_chapters)
      user = course.author
      user.update_attribute(:approved, true)
      login_user(user)

      expect { post :create, gaming_event: { top: 0, left: 0, start: 0, duration: 1,
                                             text: "text", course_chapter_id: CourseChapter.last.id }, format: :json }.to change { course.author.reload.score }.by(ScoreActivity.scores[:GamingEvent][:create])
    end

    it "gives points for discovering" do
      gaming_event = FactoryGirl.create(:gaming_event)
      user = FactoryGirl.create(:student, approved: true)
      FactoryGirl.create(:enrolment, user: user, course: Course.last)

      login_user(user)

      expect { post :discover, id: gaming_event.id }.to change { user.reload.score }.by(ScoreActivity.scores[:GamingEvent][:discover])
    end
  end
end

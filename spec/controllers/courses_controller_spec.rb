require 'spec_helper'

describe CoursesController do
  before do
    FactoryGirl.create(:course_with_chapters)
    Redis.current.flushall
  end

  context "page view counters" do
    subject { get :show, id: Course.last.id }

    it "allows anonymous views" do
      subject
      expect(response).to be_success
    end

    it "does not count anonymous views as registered" do
      expect { subject }.to_not change(Course.last.user_views, :length)
    end

    it "does not count registered views as anonymous" do
      login_user
      expect { subject }.to_not change(Course.last.anonymous_views, :length)
    end

    it "counts unique anonymous views" do
      expect { subject }.to change(Course.last.anonymous_views, :length).by(1)
      expect { subject }.to_not change(Course.last.anonymous_views, :length)
    end

    it "counts unique views by registered users" do
      login_user
      expect { subject }.to change(Course.last.user_views, :length).by(1)
      expect { subject }.to_not change(Course.last.user_views, :length)
    end
  end

  context "scores for enrolments" do
    before { login_user(FactoryGirl.create(:student, approved: true)) }
    subject { post :enrol, id: Course.last.id }

    it "gives points to student" do
      expect { subject }.to change { controller.send(:current_user).reload.score }.by(ScoreActivity.scores[:Course][:enrol])
    end

    it "gives points to course author" do
      expect { subject }.to change { Course.last.author.score }.by(ScoreActivity.scores[:Enrolment][:create])
    end
  end
end
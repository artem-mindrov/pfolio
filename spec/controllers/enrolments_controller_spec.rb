require 'spec_helper'

describe EnrolmentsController do
  context "scores" do
    it "gives points for progressing" do
      course = FactoryGirl.create(:course_with_chapters)
      user = FactoryGirl.create(:student, approved: true)
      enrolment = FactoryGirl.create(:enrolment, user: user, course: course)

      login_user(user)

      course.course_chapters.each do |c|
        expect { post :progress, id: enrolment.id, format: :js }.to change { user.reload.score }
      end
    end
  end
end
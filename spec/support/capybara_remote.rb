# SELENIUM_SERVER is the IP address or hostname of the system running Selenium
# Server, this is used to determine where to connect to when using one of the
# selenium_remote_* drivers
SELENIUM_SERVER = "192.168.33.1"

# SELENIUM_APP_HOST is the IP address or hostname of this system (where the
# tests run against) as reachable for the SELENIUM_SERVER. This is used to set
# the Capybara.app_host when using one of the selenium_remote_* drivers
SELENIUM_APP_HOST = "192.168.33.10"

# CAPYBARA_DRIVER is the Capybara driver to use, this defaults to Selenium with
# Firefox
CAPYBARA_DRIVER = "selenium_remote_firefox"

require "billy/rspec"

module Billy
  class Proxy
    def host
      SELENIUM_APP_HOST
    end
  end
end

Billy.configure do |c|
  c.whitelist = [SELENIUM_APP_HOST, 'localhost', '127.0.0.1']
end

# Determines if a selenium_remote_* driver is being used
def selenium_remote?
  !(Capybara.current_driver.to_s =~ /\Aselenium_remote/).nil?
end

def setup_remote
  if selenium_remote?
    Capybara.server_host = SELENIUM_APP_HOST
    Capybara.app_host = "http://#{SELENIUM_APP_HOST}:#{Capybara.current_session.server.port}"
    ActionMailer::Base.default_url_options = { host: SELENIUM_APP_HOST, port: Capybara.current_session.server.port }
  end
end

def teardown_remote
  Capybara.reset_sessions!
  Capybara.use_default_driver
  Capybara.app_host = nil
end

RSpec.configure do |config|
  config.before(:each) { setup_remote }
  config.after(:each) { teardown_remote }
end

# CapybaraDriverRegistrar is a helper class that enables you to easily register
# Capybara drivers
class CapybaraDriverRegistrar

  # register a Selenium driver for the given browser to run on the localhost
  def self.register_selenium_local_driver(browser)
    Capybara.register_driver "selenium_#{browser}".to_sym do |app|
      Capybara::Selenium::Driver.new(app, browser: browser)
    end
  end

  # register a Selenium driver for the given browser to run with a Selenium
  # Server on another host
  def self.register_selenium_remote_driver(browser)
    Capybara.register_driver "selenium_remote_#{browser}".to_sym do |app|
      Capybara::Selenium::Driver.new(app, browser: :remote, url: "http://#{SELENIUM_SERVER}:4444/wd/hub", desired_capabilities: browser)
    end
  end

  def self.register_selenium_remote_billy_driver
    Capybara.register_driver :selenium_remote_firefox_billy do |app|
      profile = Selenium::WebDriver::Firefox::Profile.new
      profile.proxy = Selenium::WebDriver::Proxy.new(
          http: "#{SELENIUM_APP_HOST}:#{Billy.proxy.port}",
          ssl: "#{SELENIUM_APP_HOST}:#{Billy.proxy.port}")
      Capybara::Selenium::Driver.new(app, browser: :remote,
                                     url: "http://#{SELENIUM_SERVER}:4444/wd/hub",
                                     desired_capabilities: Selenium::WebDriver::Remote::Capabilities.firefox(firefox_profile: profile))
    end
  end
end

if ENV["GUI"] == "headless"
  # Register various Selenium drivers
  CapybaraDriverRegistrar.register_selenium_remote_driver(:firefox)
  CapybaraDriverRegistrar.register_selenium_remote_billy_driver

  # At this point, Capybara.default_driver is :rack_test, and
  # Capybara.javascript_driver is :selenium. We can't run :selenium in the Vagrant box,
  # so we set the javascript driver to :selenium_remote_firefox which we're going to
  # configure.
  Capybara.javascript_driver = :selenium_remote_firefox
end

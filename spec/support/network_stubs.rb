require 'securerandom'

module NetworkStubs
  def self.included(base)
    base.class_exec do
      before(:all) { WebMock.allow_net_connect! }

      before :each, driver: :selenium_remote_firefox_billy do
        register_billy_stubs
      end
    end
  end

  def register_webmock_stubs
    stub_request(:any, %r{https?://.*api\.mailchimp\.com/}).to_return(
                         status: 200, body: ActiveSupport::JSON.encode(ok: "hi!"))
    stub_request(:post, %r{https?://.*amazonaws\.com.*uploads\z}).to_return(
                         status: 200,
                         body: ->(request) {
                           XmlSimple.xml_out(Key: [ request.uri.path.gsub(/\A\//, "") ],
                                             UploadId: [ SecureRandom.hex ])
                         }
    )
    stub_request(:post, %r{https?://.*amazonaws\.com.*uploadId=.*}).to_return(
                         status: 200,
                         body: ->(request) {
                           XmlSimple.xml_out(Location: [ request.uri.to_s ])
                         }
    )
  end

  def register_billy_stubs
    proxy.stub(/amazonaws.com.*uploadId=.*/, method: :put).and_return(
      { code: 200,
        headers:
          {
            "Access-Control-Allow-Origin" => "*",
            "Access-Control-Allow-Methods" => "PUT, GET, POST, DELETE",
            "Access-Control-Expose-Headers" => "ETag",
            "Access-Control-Max-Age" => 3000,
            "Vary" => "Origin, Access-Control-Request-Headers, Access-Control-Request-Method",
            "Etag" => SecureRandom.hex,
            "Server" => "AmazonS3"
          }
      }
    )
  end
end
module RequestHelpers
  def attach_remote_file(path, selector = "input[type=file]")
    filepath = File.join(Rails.root, path)

    # these two lines are for handling remote uploads
    filepath.gsub!(/\//, "\\") if filepath.match(/^[A-Z]:\//) || RUBY_PLATFORM =~ /mingw|mswin|cygwin/
    detector = page.driver.browser.send(:bridge).file_detector

    page.driver.browser.file_detector = ->(args){ args.first.to_s }

    attach_file find(selector)[:id], filepath

    # have to do this to avoid weird "uninitialized constant Selenium::WebDriver::Remote::Bridge::WebDriverError" exceptions in subsequent tests
    page.driver.browser.file_detector = detector
  end

  def remove_error_popups
    page.all(".formError").each { |popup| popup.click }
  end

  def have_js_validation_error_on(element)
    have_xpath("//#{element}/preceding-sibling::div[contains(@class, 'formError')][1]")
  end

  def expect_pagination_for(selector, collection, per_page)
    i1, i2 = collection.page(1).per(per_page).first, collection.page(2).per(per_page).first
    expect(page).to have_css("#{selector} .pagination")
    expect(page).to have_content(i1)
    expect(page).to_not have_content(i2)

    find("#{selector} .pagination .next a").click

    expect(page).to have_content(i2, wait: 10)
    expect(page).to_not have_content(i1)
  end

  def tab_away_from(input)
    input = find_field(input) if input.is_a?(String)
    input.native.send_keys(:tab)
  end

  def go_back
    page.evaluate_script('window.history.back()')
  end

  def go_forward
    page.evaluate_script('window.history.forward()')
  end

  def drag(source, target, right_by = 0, down_by = 0)
    page.driver.browser.action.
        click_and_hold(source.native).
        move_to(target.native, right_by, down_by).
        release.perform
  end
end
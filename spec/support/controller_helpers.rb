module ControllerHelpers
  def login_user(user = nil)
    @request.env["devise.mapping"] = Devise.mappings[:user]

    if user.nil?
      user = FactoryGirl.create(:user, approved: true)
    end

    user.confirm! unless user.confirmed?
    sign_in(user)
  end
end
require 'spec_helper'
require 'sucker_punch/testing/inline'

describe CourseChapter do
  context "background processing" do
    it "sets processing status to false when finished" do
      c = FactoryGirl.create(:course_chapter)
      c.save_and_process
      expect(c.reload.processing).to be_false
    end
  end

  it "has next and previous methods" do
    c = FactoryGirl.create(:course)
    chapters = FactoryGirl.create_list(:course_chapter, 3, course: c)

    expect(chapters.first.previous).to be_nil
    expect(chapters.last.next).to be_nil

    1.upto(2) { |i| expect(c.course_chapters[i].previous).to eq(c.course_chapters[i-1]) }
    0.upto(1) { |i| expect(c.course_chapters[i].next).to eq(c.course_chapters[i+1]) }
  end
end

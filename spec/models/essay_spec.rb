require 'spec_helper'

describe Essay do
  it "limits length to #{Essay::WORD_LIMIT} words" do
    essay = FactoryGirl.build :essay, text: Array.new(Essay::WORD_LIMIT + 1, "word").join(" ")
    expect(essay).to_not be_valid
    expect(essay).to have(1).error_on(:text)
  end

  it "allows #{Essay::WORD_LIMIT} words" do
    essay = FactoryGirl.build :essay, text: Array.new(Essay::WORD_LIMIT, "word").join(" ")
    expect(essay).to be_valid
  end
end

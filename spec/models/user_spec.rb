require 'spec_helper'

describe User do
  shared_examples_for :nameless_user do
    it { should_not validate_presence_of(:name) }
    it { should_not validate_presence_of(:bio) }
    it { should_not ensure_length_of(:name).is_at_least(3).is_at_most(48) }
    it { should_not ensure_length_of(:bio).is_at_least(16).is_at_most(256) }
  end

  context "admin" do
    subject { FactoryGirl.create(:admin) }

    it_should_behave_like :nameless_user
    it { should_not validate_presence_of(:website) }
    it { should be_approved }
    it { should be_active_for_authentication }

    it "notifies other admins" do
      u = FactoryGirl.build(:admin)
      expect(u).to receive(:notify_admin)
      u.save
    end

    it "does not send self-welcome" do
      u = FactoryGirl.build(:admin)
      expect(u).to_not receive(:send_confirmation_instructions)
      u.save
    end
  end

  context "mere mortal" do
    subject { FactoryGirl.create(:creative) }

    describe "when created" do
      it { should_not be_approved }
      it { should_not be_active_for_authentication }

      it "does not accept invalid website URLs" do
        u = FactoryGirl.build(:creative, website: nil)
        expect(u).to_not be_valid
        expect(u).to have_at_least(1).error_on(:website)

        u.website = "invalid.url"

        expect(u).to_not be_valid
        expect(u).to have_at_least(1).error_on(:website)
      end

      it "notifies other admins" do
        u = FactoryGirl.build(:creative)
        expect(u).to receive(:notify_admin)
        u.save
      end

      it "does not send self-welcome" do
        u = FactoryGirl.build(:creative)
        expect(u).to_not receive(:send_confirmation_instructions)
        u.save
      end

      it "sends a thank you message" do
        email = ActionMailer::Base.deliveries.last
        expect(email.subject).to match(/#{I18n.t("user_mailer.thanks.subject")}/i)
      end
    end

    describe "when being approved by admin" do
      before(:each) { subject.approved = true }

      it { should be_approved }
      it { should_not be_active_for_authentication }
    end

    describe "after confirmation" do
      before(:each) do
        subject.update_attributes(approved: true, name: "name", bio: "some kind of a biography")
        subject.confirm!
      end

      it { should ensure_length_of(:name).is_at_least(3).is_at_most(48) }
      it { should ensure_length_of(:bio).is_at_least(16).is_at_most(256) }
    end
  end
end

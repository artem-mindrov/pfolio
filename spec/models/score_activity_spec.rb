require 'spec_helper'

describe ScoreActivity do
  it "gets daily score reports" do
    user = FactoryGirl.create(:creative)

    2.times do
      FactoryGirl.create(:course_with_chapters, author: user)
      Course.last.course_chapters.each {|c| user.score_activities.create(subject: c, action: "create") }

      Timecop.travel(2.days.from_now)
    end

    report = user.score_activities.daily

    expect(report.to_a.size).to eq(2)
    report.each do |item|
      course = CourseChapter.find(item.subject_ids.first).course
      expect(item.subject_ids).to match_array(course.course_chapter_ids)
    end
  end
end

require 'spec_helper'

describe GamingEvent do
  context "basic validations" do
    subject { FactoryGirl.create(:gaming_event) }

    [:top, :left].each do |attr|
      it { should_not allow_value(-1).for(attr) }
      it { should_not allow_value(100).for(attr) }
      it { should_not allow_value("abc").for(attr) }
      it { should allow_value(99).for(attr) }
    end

    it { should_not allow_value(-1).for(:start) }
    it { should_not allow_value(0).for(:duration) }
  end

  it "does not allow more than #{CourseChapter::EVENT_LIMIT} events per chapter" do
    c = FactoryGirl.create(:course_chapter)
    FactoryGirl.create_list(:gaming_event, CourseChapter::EVENT_LIMIT, course_chapter: c)

    e = FactoryGirl.build(:gaming_event, course_chapter: c)
    expect(e).to_not be_valid
    expect(e).to have_at_least(1).error_on(:base)
  end

  context "scopes" do
    before do
      c = FactoryGirl.create(:course_chapter)
      FactoryGirl.create(:enrolment, course: c.course)
      @discovered_event = FactoryGirl.create(:gaming_event, course_chapter: CourseChapter.last)
      FactoryGirl.create(:discovery, gaming_event: @discovered_event, enrolment: Enrolment.last)
      @undiscovered_event = FactoryGirl.create(:gaming_event, course_chapter: CourseChapter.last)
      @events = CourseChapter.last.gaming_events
    end

    context "discovered" do
      it "returns undiscovered events for the chapter" do
        expect(@events.discovered(Enrolment.last)).to_not include(@undiscovered_event)
      end

      it "returns discovered events for the chapter" do
        expect(@events.discovered(Enrolment.last)).to include(@discovered_event)
      end
    end

    context "undiscovered" do
      it "does not return discovered events for the chapter" do
        expect(@events.undiscovered(Enrolment.last)).to include(@undiscovered_event)
      end

      it "does not return discovered events for the chapter" do
        expect(@events.undiscovered(Enrolment.last)).to_not include(@discovered_event)
      end
    end
  end
end

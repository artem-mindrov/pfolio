require 'spec_helper'
require 'sucker_punch/testing/inline'

describe Course do
  context "viewable scope" do
    it "does not include empty courses" do
      c = FactoryGirl.create(:course)
      expect(Course.viewable).to_not include(c)
    end

    it "includes courses that have completed processing" do
      c = FactoryGirl.create(:course_with_chapters)
      expect(Course.viewable).to include(c)
    end

    it "does not include courses that are being processed" do
      c = FactoryGirl.create(:course_with_chapters, processing: true)
      expect(Course.viewable).to_not include(c)
    end

    it "does not include courses with chapters that are being processed" do
      c = FactoryGirl.create(:course_with_incomplete_chapters)
      expect(Course.viewable).to_not include(c)
    end
  end

  context "background processing" do
    it "sets processing status to false when finished" do
      c = FactoryGirl.create(:course)
      c.save_and_process
      expect(c.reload.processing).to be_false
    end
  end

  it "reorders chapters according to new position values" do
    c = FactoryGirl.create(:course_with_chapters)
    chapters = c.course_chapters
    new_chapter_ids = chapters.pluck(:id).reverse
    c.reorder Hash[chapters.pluck(:id).zip(chapters.pluck(:position).reverse)]
    expect(c.reload.course_chapter_ids).to eq(new_chapter_ids)
  end
end

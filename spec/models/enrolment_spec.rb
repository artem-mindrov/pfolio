require 'spec_helper'

describe Enrolment do
  let(:course) { FactoryGirl.create(:course_with_chapters) }
  let(:enrolment) { FactoryGirl.create(:enrolment, course: course)}

  context "scopes" do
    before { FactoryGirl.create(:enrolment, course: course, progress: course.course_chapters.count) }

    it "returns completed enrolments" do
      expect(Enrolment.complete).to_not include(enrolment)
      expect(Enrolment.complete.count).to eq(1)
    end

    it "returns incomplete enrolments" do
      expect(Enrolment.incomplete).to include(enrolment)
      expect(Enrolment.incomplete.count).to eq(1)
    end
  end

  context "validations" do
    it "does not allow progressing beyond course length" do
      enrolment.progress = enrolment.course.course_chapters.count + 1
      expect(enrolment).to_not be_valid
      expect(enrolment).to have(1).error_on(:progress)
    end
  end
end

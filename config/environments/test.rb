Curio::Application.configure do
  # Settings specified here will take precedence over those in config/application.rb

  # The test environment is used exclusively to run your application's
  # test suite. You never need to work with it otherwise. Remember that
  # your test database is "scratch space" for the test suite and is wiped
  # and recreated between test runs. Don't rely on the data there!
  config.cache_classes = true

  # Configure static asset server for tests with Cache-Control for performance
  config.serve_static_assets = true
  config.static_cache_control = "public, max-age=3600"

  # Show full error reports and disable caching
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = false

  # Raise exceptions instead of rendering exception templates
  config.action_dispatch.show_exceptions = false

  # Disable request forgery protection in test environment
  config.action_controller.allow_forgery_protection    = false

  # Tell Action Mailer not to deliver emails to the real world.
  # The :test delivery method accumulates sent emails in the
  # ActionMailer::Base.deliveries array.
  config.action_mailer.delivery_method = :test

  config.action_mailer.default_url_options = { host: "localhost", port: 3000 }

  # Print deprecation notices to the stderr
  config.active_support.deprecation = :stderr

  config.eager_load = false

  Fog.mock!

  def fog_directory
    "picct"
  end

  connection = ::Fog::Storage.new(
    aws_access_key_id: "xxx",
    aws_secret_access_key: "yyy",
    provider: 'AWS',
    region: 'eu-west-1'
  )

  connection.directories.create(key: fog_directory)

  S3Multipart.configure do |config|
    config.bucket_name   = fog_directory
    config.s3_access_key = "xxx"
    config.s3_secret_key = "yyy"
    config.revision      = "0.0.10.4"
  end

  CarrierWave.configure do |c|
    c.fog_credentials = {
      provider:              'AWS',
      aws_access_key_id:     'xxx',
      aws_secret_access_key: 'yyy',
      region:                'eu-west-1'
    }
    c.fog_directory = fog_directory
    c.enable_processing = false
    c.validate_download = false
  end

  #config.logger = Logger.new(STDOUT)
end

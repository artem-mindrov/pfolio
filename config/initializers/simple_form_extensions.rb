module SimpleForm
  class FormBuilder
    def generic_error(options = {})
      messages = object.errors[:base]

      if messages.present?
        options[:class] = "#{SimpleForm.error_notification_class} #{options[:class]}".strip

        template.content_tag(SimpleForm.error_notification_tag, options) do
          template.content_tag(:ul) do
            messages.inject("".html_safe) do |content, message|
              content << template.content_tag(:li, message)
            end
          end
        end
      end
    end
  end
end
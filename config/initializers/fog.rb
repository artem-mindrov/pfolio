if %w{ development }.include?(Rails.env)
  require 'fog/aws/models/storage/files'

  # fog always expects Last-Modified and ETag headers to present
  # We relax this requirement to support fakes3
  class Fog::Storage::AWS::Files
    def normalise_headers(headers)
      headers['Last-Modified'] = Time.parse(headers['Last-Modified']) if headers['Last-Modified']
      headers['ETag'].gsub!('"','') if headers['ETag']
    end
  end

  module Fog::Storage::AWS::Utils
    def request_params_with_fakes3(params)
      if @endpoint
        path = params[:object_name] ? object_to_path(params[:object_name]) : CGI.unescape(params[:path])
        path = '/' + path if path[0..0] != '/'
        path = bucket_to_path(params[:bucket_name], path)
        path = CGI.escape(path) if params[:method] == "DELETE"

        params = request_params_without_fakes3(params)

        params[:host] = if params[:region]
          region_to_host(params[:region])
        else
          @host || region_to_host(@region || DEFAULT_REGION)
        end

        params[:path] = path
        params
      else
        request_params_without_fakes3(params)
      end
    end

    alias_method_chain :request_params, :fakes3
  end
end

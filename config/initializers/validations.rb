module ActiveModel::Validations::HelperMethods
  def validates_url(*attr_names)
    validates_with UrlValidator, _merge_attributes(attr_names)
  end
end
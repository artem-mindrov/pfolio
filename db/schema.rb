# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140327114843) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "chapter_references", force: true do |t|
    t.integer  "course_chapter_id"
    t.string   "title",             null: false
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "chapter_references", ["course_chapter_id"], name: "index_chapter_references_on_course_chapter_id", using: :btree

  create_table "course_categories", force: true do |t|
    t.integer "course_id"
    t.integer "topic_id"
  end

  add_index "course_categories", ["course_id", "topic_id"], name: "index_course_categories_on_course_id_and_topic_id", unique: true, using: :btree

  create_table "course_chapters", force: true do |t|
    t.integer  "course_id"
    t.string   "title",                                        null: false
    t.text     "description"
    t.string   "video",                                        null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "position",               limit: 8, default: 0, null: false
    t.boolean  "processing"
    t.integer  "s3_multipart_upload_id"
  end

  add_index "course_chapters", ["course_id"], name: "index_course_chapters_on_course_id", using: :btree
  add_index "course_chapters", ["position"], name: "index_course_chapters_on_position", using: :btree
  add_index "course_chapters", ["processing"], name: "index_course_chapters_on_processing", using: :btree

  create_table "courses", force: true do |t|
    t.integer  "author_id",              null: false
    t.string   "title",                  null: false
    t.string   "intro"
    t.text     "description"
    t.decimal  "fee"
    t.string   "level"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "processing"
    t.integer  "s3_multipart_upload_id"
    t.string   "slug"
  end

  add_index "courses", ["author_id"], name: "index_courses_on_author_id", using: :btree
  add_index "courses", ["processing"], name: "index_courses_on_processing", using: :btree
  add_index "courses", ["slug"], name: "index_courses_on_slug", unique: true, using: :btree

  create_table "discoveries", force: true do |t|
    t.integer  "gaming_event_id"
    t.integer  "enrolment_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "enrolments", force: true do |t|
    t.integer  "user_id"
    t.integer  "course_id"
    t.integer  "progress",     default: 0, null: false
    t.datetime "completed_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "enrolments", ["completed_at"], name: "index_enrolments_on_completed_at", using: :btree
  add_index "enrolments", ["user_id", "course_id"], name: "index_enrolments_on_user_id_and_course_id", unique: true, using: :btree

  create_table "gaming_events", force: true do |t|
    t.integer  "course_chapter_id"
    t.integer  "top",               limit: 2, default: 0, null: false
    t.integer  "left",              limit: 2, default: 0, null: false
    t.integer  "start",             limit: 2, default: 0, null: false
    t.integer  "duration",          limit: 2, default: 1, null: false
    t.text     "text"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "gaming_events", ["course_chapter_id"], name: "index_gaming_events_on_course_chapter_id", using: :btree

  create_table "inquiry_answers", force: true do |t|
    t.integer  "question_id"
    t.text     "text"
    t.text     "help_text"
    t.integer  "display_order"
    t.string   "answer_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "inquiry_attempts", force: true do |t|
    t.integer  "respondent_id"
    t.string   "respondent_type"
    t.integer  "survey_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "inquiry_questions", force: true do |t|
    t.integer  "survey_id"
    t.text     "text"
    t.text     "help_text"
    t.string   "display_type"
    t.boolean  "required",            default: false
    t.boolean  "allow_user_response", default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "inquiry_responses", force: true do |t|
    t.integer  "question_id"
    t.integer  "answer_id"
    t.integer  "attempt_id"
    t.integer  "row_id"
    t.integer  "column_id"
    t.text     "text_value"
    t.datetime "datetime_value"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "inquiry_surveys", force: true do |t|
    t.string   "title"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "interests", force: true do |t|
    t.integer "user_id"
    t.integer "topic_id"
  end

  create_table "portfolio_items", force: true do |t|
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.string   "description"
    t.integer  "user_id"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.boolean  "primary",            default: false
  end

  create_table "price_ranges", force: true do |t|
    t.integer  "evaluable_id"
    t.string   "evaluable_type"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "price_tags", force: true do |t|
    t.integer  "evaluable_id"
    t.string   "evaluable_type"
    t.integer  "price_cents"
    t.string   "currency",       default: "GBP"
    t.string   "description"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  add_index "price_tags", ["currency"], name: "index_price_tags_on_currency", using: :btree
  add_index "price_tags", ["price_cents"], name: "index_price_tags_on_price_cents", using: :btree

  create_table "roles", force: true do |t|
    t.string   "name"
    t.integer  "resource_id"
    t.string   "resource_type"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "roles", ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id", using: :btree
  add_index "roles", ["name"], name: "index_roles_on_name", using: :btree

  create_table "s3_multipart_uploads", force: true do |t|
    t.string   "location"
    t.string   "upload_id"
    t.string   "key"
    t.string   "name"
    t.string   "uploader"
    t.integer  "size"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "score_activities", force: true do |t|
    t.integer  "owner_id",               null: false
    t.integer  "subject_id"
    t.string   "subject_type"
    t.integer  "score",        limit: 2
    t.string   "action",                 null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "score_activities", ["created_at"], name: "index_score_activities_on_created_at", using: :btree
  add_index "score_activities", ["owner_id"], name: "index_score_activities_on_owner_id", using: :btree
  add_index "score_activities", ["subject_id", "subject_type"], name: "index_score_activities_on_subject_id_and_subject_type", using: :btree
  add_index "score_activities", ["subject_type", "action"], name: "index_score_activities_on_subject_type_and_action", using: :btree

  create_table "taggings", force: true do |t|
    t.integer  "tag_id"
    t.integer  "taggable_id"
    t.string   "taggable_type"
    t.integer  "tagger_id"
    t.string   "tagger_type"
    t.string   "context",       limit: 128
    t.datetime "created_at"
  end

  add_index "taggings", ["tag_id"], name: "index_taggings_on_tag_id", using: :btree
  add_index "taggings", ["taggable_id", "taggable_type", "context"], name: "index_taggings_on_taggable_id_and_taggable_type_and_context", using: :btree

  create_table "tags", force: true do |t|
    t.string "name"
  end

  create_table "topics", force: true do |t|
    t.string   "name",       null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "topics", ["name"], name: "index_topics_on_name", unique: true, using: :btree

  create_table "users", force: true do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.boolean  "approved",               default: false
    t.string   "name"
    t.string   "bio"
    t.string   "website"
    t.string   "slug"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.string   "avatar"
    t.integer  "score",                  default: 0,     null: false
  end

  add_index "users", ["approved"], name: "index_users_on_approved", using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["score"], name: "index_users_on_score", using: :btree
  add_index "users", ["slug"], name: "index_users_on_slug", unique: true, using: :btree

  create_table "users_roles", id: false, force: true do |t|
    t.integer "user_id"
    t.integer "role_id"
  end

  add_index "users_roles", ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id", using: :btree

end

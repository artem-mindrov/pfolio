class CreateEnrolments < ActiveRecord::Migration
  def change
    create_table :enrolments do |t|
      t.references :user
      t.references :course
      t.integer :progress, null: false, default: 0
      t.datetime :completed_at
      t.timestamps
    end

    add_index :enrolments, [:user_id, :course_id], unique: true
    add_index :enrolments, :completed_at
  end
end

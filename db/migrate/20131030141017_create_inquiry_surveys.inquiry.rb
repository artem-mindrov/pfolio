# This migration comes from inquiry (originally 20130915164752)
class CreateInquirySurveys < ActiveRecord::Migration
  def change
    create_table :inquiry_surveys do |t|
      t.string :title
      t.text :description
      t.timestamps
    end
  end
end

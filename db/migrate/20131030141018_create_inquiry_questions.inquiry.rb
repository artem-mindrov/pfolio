# This migration comes from inquiry (originally 20130915164840)
class CreateInquiryQuestions < ActiveRecord::Migration
  def change
    create_table :inquiry_questions do |t|
      t.references :survey

      t.text :text
      t.text :help_text
      t.string :display_type
      t.boolean :required, default: false
      t.boolean :allow_user_response, default: false
      t.timestamps
    end
  end
end

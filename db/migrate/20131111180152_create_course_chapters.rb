class CreateCourseChapters < ActiveRecord::Migration
  def change
    create_table :course_chapters do |t|
      t.references :course
      t.string :title, null: false
      t.text :description
      t.string :video, null: false
      t.timestamps
    end

    add_index :course_chapters, :course_id

    create_table :chapter_references do |t|
      t.references :course_chapter
      t.string :title, null: false
      t.string :description
      t.timestamps
    end

    add_index :chapter_references, :course_chapter_id
  end
end

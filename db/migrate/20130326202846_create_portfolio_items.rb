class CreatePortfolioItems < ActiveRecord::Migration
  def change
    create_table :portfolio_items do |t|
      t.attachment :image
      t.string :description
      t.references :user
      t.timestamps
    end
  end
end

class AddFieldsToUsers < ActiveRecord::Migration
  def change
    change_table :users do |t|
      t.string :name
      t.string :bio
      t.string :website
      t.string :slug
    end

    add_index :users, :slug, unique: true
  end
end

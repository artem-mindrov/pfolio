class CreateCourses < ActiveRecord::Migration
  def change
    create_table :courses do |t|
      t.integer :author_id, null: false
      t.string :title, null: false
      t.string :intro
      t.text :description
      t.decimal :fee
      t.string :level
      t.timestamps
    end

    add_index :courses, :author_id

    create_table :course_categories do |t|
      t.references :course
      t.references :topic
    end

    add_index :course_categories, [:course_id, :topic_id], unique: true
  end
end

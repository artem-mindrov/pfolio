class AddUploaderToCourseChapter < ActiveRecord::Migration
  def change
    change_table :course_chapters do |t|
      t.integer :s3_multipart_upload_id
    end
  end
end

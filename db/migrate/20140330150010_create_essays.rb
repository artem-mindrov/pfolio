class CreateEssays < ActiveRecord::Migration
  def change
    create_table :essays do |t|
      t.integer :author_id, null: false
      t.string :title, null: false
      t.text :text, null: false
      t.string :slug
      t.timestamps
    end

    add_index :essays, :author_id
    add_index :essays, :slug, unique: true
  end
end

class AddPrimaryToPortfolioItems < ActiveRecord::Migration
  def change
    add_column :portfolio_items, :primary, :boolean, default: false

    User.all.each do |user|
      user.portfolio_items.first.update_attribute(:primary, true) if user.portfolio_items.present?
    end
  end
end

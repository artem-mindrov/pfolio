class CreateTopics < ActiveRecord::Migration
  def change
    create_table :topics do |t|
      t.string :name, null: false
      t.timestamps
    end

    add_index :topics, :name, unique: true

    %w{ Photography Illustration Drawing Printmaking }.each do |topic|
      Topic.create(name: topic)
    end

    create_table :interests do |t|
      t.references :user
      t.references :topic
    end
  end
end

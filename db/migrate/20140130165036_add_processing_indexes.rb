class AddProcessingIndexes < ActiveRecord::Migration
  def change
    add_index :courses, :processing
    add_index :course_chapters, :processing
  end
end

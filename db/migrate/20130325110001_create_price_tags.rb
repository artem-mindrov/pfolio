class CreatePriceTags < ActiveRecord::Migration
  def change
    create_table :price_tags do |t|
      t.integer :evaluable_id
      t.string :evaluable_type
      t.integer :price_cents
      t.string :currency, default: "GBP"
      t.string :description
      t.timestamps
    end

    add_index :price_tags, :price_cents
    add_index :price_tags, :currency
  end
end

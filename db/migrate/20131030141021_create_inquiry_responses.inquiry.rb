# This migration comes from inquiry (originally 20130917084543)
class CreateInquiryResponses < ActiveRecord::Migration
  def change
    create_table :inquiry_responses do |t|
      t.references :question
      t.references :answer
      t.references :attempt

      t.integer :row_id, references: :inquiry_answers
      t.integer :column_id, references: :inquiry_answers

      t.text :text_value
      t.datetime :datetime_value
      t.timestamps
    end
  end
end

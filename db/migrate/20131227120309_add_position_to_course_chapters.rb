class AddPositionToCourseChapters < ActiveRecord::Migration
  def change
    add_column :course_chapters, :position, :integer, limit: 8, null: false, default: 0
    add_index :course_chapters, :position
  end
end

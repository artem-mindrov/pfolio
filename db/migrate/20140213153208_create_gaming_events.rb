class CreateGamingEvents < ActiveRecord::Migration
  def change
    create_table :gaming_events do |t|
      t.references :course_chapter
      t.integer :top, limit: 2, null: false, default: 0
      t.integer :left, limit: 2, null: false, default: 0
      t.integer :start, limit: 2, null: false, default: 0
      t.integer :duration, limit: 2, null: false, default: 1
      t.text :text
      t.timestamps
    end

    add_index :gaming_events, :course_chapter_id
  end
end

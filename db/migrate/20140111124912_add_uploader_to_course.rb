class AddUploaderToCourse < ActiveRecord::Migration
  def change
    change_table :courses do |t|
      t.integer :s3_multipart_upload_id
    end
  end
end

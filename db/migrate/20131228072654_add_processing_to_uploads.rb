class AddProcessingToUploads < ActiveRecord::Migration
  def change
    add_column :courses, :processing, :boolean
    add_column :course_chapters, :processing, :boolean
  end
end

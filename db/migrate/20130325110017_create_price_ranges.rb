class CreatePriceRanges < ActiveRecord::Migration
  def change
    create_table :price_ranges do |t|
      t.integer :evaluable_id
      t.string :evaluable_type
      t.timestamps
    end
  end
end

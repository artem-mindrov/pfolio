# This migration comes from inquiry (originally 20130917084441)
class CreateInquiryAttempts < ActiveRecord::Migration
  def change
    create_table :inquiry_attempts do |t|
      t.belongs_to :respondent, polymorphic: true
      t.references :survey
      t.timestamps
    end
  end
end

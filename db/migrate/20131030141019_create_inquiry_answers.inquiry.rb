# This migration comes from inquiry (originally 20130915165025)
class CreateInquiryAnswers < ActiveRecord::Migration
  def change
    create_table :inquiry_answers do |t|
      t.references :question
      t.text :text
      t.text :help_text
      t.integer :display_order
      t.string :answer_type
      t.timestamps
    end
  end
end

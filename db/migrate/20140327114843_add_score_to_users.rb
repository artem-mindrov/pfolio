class AddScoreToUsers < ActiveRecord::Migration
  def change
    add_column :users, :score, :integer, null: false, default: 0
    add_index :users, :score
  end
end

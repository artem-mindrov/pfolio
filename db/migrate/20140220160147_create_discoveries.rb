class CreateDiscoveries < ActiveRecord::Migration
  def change
    create_table :discoveries do |t|
      t.references :gaming_event
      t.references :enrolment
      t.timestamps
    end
  end
end

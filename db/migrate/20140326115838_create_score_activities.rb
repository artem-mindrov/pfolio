class CreateScoreActivities < ActiveRecord::Migration
  def change
    create_table :score_activities do |t|
      t.integer :owner_id, null: false
      t.belongs_to :subject, polymorphic: true
      t.integer :score, limit: 2
      t.string :action, null: false
      t.timestamps
    end

    add_index :score_activities, :owner_id
    add_index :score_activities, [:subject_id, :subject_type]
    add_index :score_activities, [:subject_type, :action]
    add_index :score_activities, :created_at
  end
end

source 'http://rubygems.org'
ruby '2.0.0'

gem 'rails', '~> 4.0.0'
gem 'puma'

# Bundle edge Rails instead:
# gem 'rails', :git => 'git://github.com/rails/rails.git'

gem 'pg'
gem 'kaminari'
gem 'simple_form'
#gem 'exception_notification'
gem 'jbuilder'
gem 'select2-rails'
#gem 'highline'
gem 'nested_form', github: 'ryanb/nested_form'
gem 'ransack'
gem 'redis-objects'

gem 'devise'
gem 'cancan'
gem 'rolify'
gem 'inherited_resources'
gem 'underscore-rails'
gem 'acts-as-taggable-on'
gem 'friendly_id', "5.0.0rc2"
gem 'paloma'
gem 'mobvious'

gem 'carrierwave'
gem 'carrierwave_direct', github: "artem-mindrov/carrierwave_direct"
gem 'carrierwave-video'

# https://github.com/rheaton/carrierwave-video/issues/23
# https://github.com/streamio/streamio-ffmpeg/commit/92314801af58a8ca64da17c716bce3600f234870
gem 'streamio-ffmpeg', github: "streamio/streamio-ffmpeg", ref: "9231480"

gem 'sprockets', "<= 2.11.0" # https://github.com/sstephenson/sprockets/issues/537

gem 'mini_magick'
gem 'sucker_punch', '~> 1.0'
gem 's3_multipart', github: "artem-mindrov/s3_multipart"

gem 'gibbon'

group :development do
  gem 'better_errors'
end

gem 'sass-rails'
gem 'coffee-rails'
gem 'bootstrap-sass'
gem 'font-awesome-rails'
gem 'uglifier', '>= 1.3.0'

gem 'jquery-rails'

group :development, :test do
  gem 'rspec'
  gem 'rspec-rails'
  gem 'jasminerice', github: "bradphelan/jasminerice"
  gem 'byebug'
end

group :test do
  gem 'factory_girl_rails'
  gem 'shoulda-matchers' # https://github.com/thoughtbot/shoulda-matchers/issues/259
  gem 'database_cleaner'
  gem 'capybara'
  gem "selenium-webdriver"
  gem "webmock"
  gem 'timecop'
  gem "puffing-billy", github: "artem-mindrov/puffing-billy", platform: :ruby
end

group :production do
  gem 'execjs'
  gem 'memcachier'
  gem 'dalli'
end

# To use ActiveModel has_secure_password
# gem 'bcrypt-ruby', '~> 3.0.0'

# Use unicorn as the app server
# gem 'unicorn'

# Deploy with Capistrano
# gem 'capistrano'

# To use debugger
# gem 'debugger'
